var base_url = $('#base_url').val();
$(document).ready(function() {
    graficas_mes();
    graficas_ventas_mes();
    grafica_doctores();
    clientes_top_10();
    grafica_vacunas();
    vacunas_top_v_10();
});

function get_mes_data(){
    var pro =[];
    $.ajax({
        type:'POST',
        url: base_url+'Inicio/get_data_factura_mes',
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
                pro.push(array.mes1,array.mes2,array.mes3,array.mes4,array.mes5,array.mes6,array.mes7,array.mes8,array.mes9,array.mes10,array.mes11,array.mes12);
        }
    });
    return pro;
}


function graficas_mes(){
    var data_mes=get_mes_data();
    var barData = {
        labels: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Nobiembre","Diciembre"],
        datasets: [{
            label: "Facturas realizadas",
            fillColor: "rgba(23 112 170 / 50%)",
            strokeColor: vihoAdminConfig.primary,
            highlightFill: "rgba(23 112 170)",
            highlightStroke: vihoAdminConfig.primary,
            data: data_mes
            //data: [35, 59, 80, 81, 56, 55, 40,80, 81, 56, 55, 40]
        }]
    };
    var barOptions = {
        scaleBeginAtZero: true,
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,0.1)",
        scaleGridLineWidth: 1,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke: true,
        barStrokeWidth: 2,
        barValueSpacing: 5,
        barDatasetSpacing: 1,
        responsive: true,
        showTooltips: false,
        onAnimationComplete: function() {

            var ctx = this.chart.ctx;
            ctx.font = this.scale.font;
            ctx.fillStyle = this.scale.textColor
            ctx.textAlign = "center";
            ctx.textBaseline = "bottom";

            this.datasets.forEach(function(dataset) {
              dataset.bars.forEach(function(bar) {
                ctx.fillText(bar.value, bar.x, bar.y - 1);
              });
            })
        },
    };
    var barCtx = document.getElementById("chart_mes").getContext("2d");
    var myBarChart = new Chart(barCtx).Bar(barData, barOptions);
    var polarData = [
        {
            value: 300,
            color: vihoAdminConfig.primary,
            highlight: "rgba(36, 105, 92, 1)",
            label: "Yellow"
        }, {
            value: 50,
            color: vihoAdminConfig.secondary,
            highlight: "rgba(186, 137, 93, 1)",
            label: "Sky"
        }, {
            value: 100,
            color: "#222222",
            highlight: "rgba(34,34,34,1)",
            label: "Black"
        }, {
            value: 40,
            color: "#717171",
            highlight: "rgba(113, 113, 113, 1)",
            label: "Grey"
        }, {
            value: 120,
            color: "#ff8d42",
            highlight: "#616774",
            label: "Dark Grey"
        }
    ];
}


function get_mes_data_ventas(){
    var pro =[];
    $.ajax({
        type:'POST',
        url: base_url+'Inicio/get_data_factura_ventas_mes',
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
                pro.push(array.mes1,array.mes2,array.mes3,array.mes4,array.mes5,array.mes6,array.mes7,array.mes8,array.mes9,array.mes10,array.mes11,array.mes12);
        }
    });
    return pro;
}


function graficas_ventas_mes(){
    var data_mes=get_mes_data_ventas();
    var barData = {
        labels: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Nobiembre","Diciembre"],
        datasets: [{
            label: "Facturas realizadas",
            fillColor: "rgba(23 112 170 / 50%)",
            strokeColor: vihoAdminConfig.primary,
            highlightFill: "rgba(23 112 170)",
            highlightStroke: vihoAdminConfig.primary,
            data: data_mes
            //data: [35, 59, 80, 81, 56, 55, 40,80, 81, 56, 55, 40]
        }]
    };
    var barOptions = {
        scaleBeginAtZero: true,
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,0.1)",
        scaleGridLineWidth: 1,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke: true,
        barStrokeWidth: 2,
        barValueSpacing: 5,
        barDatasetSpacing: 1,
        responsive: true,
        showTooltips: false,
        onAnimationComplete: function() {

            var ctx = this.chart.ctx;
            ctx.font = this.scale.font;
            ctx.fillStyle = this.scale.textColor
            ctx.textAlign = "center";
            ctx.textBaseline = "bottom";

            this.datasets.forEach(function(dataset) {
              dataset.bars.forEach(function(bar) {
                ctx.fillText(bar.value, bar.x, bar.y - 5);
              });
            })
        }
    };
    var barCtx = document.getElementById("chart_anio").getContext("2d");
    var myBarChart = new Chart(barCtx).Bar(barData, barOptions);
    var polarData = [
        {
            value: 300,
            color: vihoAdminConfig.primary,
            highlight: "rgba(36, 105, 92, 1)",
            label: "Yellow"
        }, {
            value: 50,
            color: vihoAdminConfig.secondary,
            highlight: "rgba(186, 137, 93, 1)",
            label: "Sky"
        }, {
            value: 100,
            color: "#222222",
            highlight: "rgba(34,34,34,1)",
            label: "Black"
        }, {
            value: 40,
            color: "#717171",
            highlight: "rgba(113, 113, 113, 1)",
            label: "Grey"
        }, {
            value: 120,
            color: "#ff8d42",
            highlight: "#616774",
            label: "Dark Grey"
        }
    ];
}

function get_top_10_data(){
    var pro =[];
    $.ajax({
        type:'POST',
        url: base_url+'Inicio/get_data_top_10_clientes',
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            array.forEach(function(element){
              pro.push({x: element.razon_social,y: element.reg});
            });
        }
    });
    return pro;
}


function grafica_doctores(){
    var data_top=get_top_10_data();
    var options55 = {
      series: [{
          name: "Médicos",
          data: data_top
      }],
      chart: {
          height: 250,
          type: "bar",
          toolbar: {
              show: false,
          },
      },
      plotOptions: {
          bar: {
              horizontal: false,
              columnWidth: "30%",
              startingShape: "rounded",
              endingShape: "rounded",
              colors: {
                  backgroundBarColors: ["#e5edef"],
                  backgroundBarOpacity: 1,
                  backgroundBarRadius: 9
              }
          },
      },
      stroke: {
          show: false,
      },
      dataLabels: {
          enabled: false
      },
      fill: {
          opacity: 1
      },
      xaxis: {
          // type: "datetime",
          axisBorder: {
              show: false
          },
          labels: {
              show: true,
              style: {
                  fontSize: '6px',
              },
          },
          axisTicks: {
              show: false,
          },
      },
      yaxis: {
          labels: {
              show: false,
          }
      },
      colors: [vihoAdminConfig.primary]
    };
    var chart55 = new ApexCharts(document.querySelector("#chart_doctores_10"), options55);
    chart55.render();
    /*
    data: [{
              x: "Jan",
              y: 500
          }, {
              x: "Feb",
              y: 3000
          }, {
              x: "Mar",
              y: 1800
          }, {
              x: "Apr",
              y: 3000,
          }, {
              x: "May",
              y: 1800
          }, {
              x: "Jun",
              y: 1500
          }, {
              x: "Jul",
              y: 2500
          }, {
              x: "Sep",
              y: 1500,
          }, {
              x: "Oct",
              y: 2000
          }, {
              x: "nom",
              y: 2000
          }]
    */
}

function clientes_top_10(){
    $.ajax({
        type:'POST',
        url: base_url+'Inicio/get_cliente_top_10_tabla',
        data: {
          anio : $('#fecha1 option:selected').val(),
          mes: $('#fecha2 option:selected').val(),
        },
        statusCode:{
            404: function(data){
                //Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                //Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
          $('.tabla_top_10_cliente').html(data);
        }
    });
}
//===========

function get_top_v_10_data(){
    var pro =[];
    $.ajax({
        type:'POST',
        url: base_url+'Inicio/get_data_top_10_vacunas',
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            array.forEach(function(element){
              pro.push({x: element.Descripcion,y: element.reg});
            });
        }
    });
    return pro;
}


function grafica_vacunas(){
    var data_top=get_top_v_10_data();
    var options55 = {
      series: [{
          name: "Vacunas",
          data: data_top
      }],
      chart: {
          height: 250,
          type: "bar",
          toolbar: {
              show: false,
          },
      },
      plotOptions: {
          bar: {
              horizontal: false,
              columnWidth: "30%",
              startingShape: "rounded",
              endingShape: "rounded",
              colors: {
                  backgroundBarColors: ["#e5edef"],
                  backgroundBarOpacity: 1,
                  backgroundBarRadius: 9
              }
          },
      },
      stroke: {
          show: false,
      },
      dataLabels: {
          enabled: false
      },
      fill: {
          opacity: 1
      },
      xaxis: {
          // type: "datetime",
          axisBorder: {
              show: false
          },
          labels: {
              show: true,
              style: {
                  fontSize: '6px',
              },
          },
          axisTicks: {
              show: false,
          },
      },
      yaxis: {
          labels: {
              show: false,
          }
      },
      colors: [vihoAdminConfig.primary]
    };
    var chart55 = new ApexCharts(document.querySelector("#chart_vacunas_10"), options55);
    chart55.render();
}

function vacunas_top_v_10(){
    $.ajax({
        type:'POST',
        url: base_url+'Inicio/get_vacunas_top_10_tabla',
        data: {
          anio : $('#fechav1 option:selected').val(),
          mes: $('#fechav2 option:selected').val(),
        },
        statusCode:{
            404: function(data){
                //Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                //Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
          $('.tabla_top_10_vacunas').html(data);
        }
    });
}