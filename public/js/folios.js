var base_url = $('#base_url').val();
var table;
var envionfactura;
var enviocliente;
$(document).ready(function($) {
	table = $('#tabla_facturacion').DataTable();
	loadtable();
	$('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Timbrado/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.clienteId,
                        text: element.razon_social,
                        rfc: element.rfc

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $('.enviocorreomail').click(function(event) {
        var DATAr  = [];
        var TABLA   = $("#table_mail_envios tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["correo"] = $(this).find("input[id*='mailcli']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arraycorreos   = JSON.stringify(DATAr);

        $.ajax({
            type:'POST',
            url: base_url+"index.php/Envio/enviocorreo",
            data: {
                factura:envionfactura,
                correo:arraycorreos
            },
            success:function(response){  
                console.log(response);
                Swal.fire({
                                position: "center",
                                icon: "success",
                                title: "Email",
                                text: 'Correo enviado',
                                showConfirmButton: false,
                                timer: 2500
                            });
                loadtable();
                $('#modalseleccioncorreo').modal('hide');
            }
        });
    });

    setTimeout(function(){ 
        $('#idclientes').val('');
    }, 1000);
});
function loadtable(){
	var facturas_star = $('#facturas_star').is(':checked')==true?1:0;
    var idcliente=$('#idcliente option:selected').val()==undefined?0:$('#idcliente option:selected').val();
    var inicio=$('#finicial').val();
    var fin=$('#finicial').val();
    var estatus_v=$('#estatus_v option:selected').val();
	table.destroy();
	table = $('#tabla_facturacion').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Folios/getlistfacturas",
            type: "post",
            "data": {
                'cliente':idcliente,
                'finicio':inicio,
                'ffin':fin,
                'star':facturas_star,
                'estatus_v':estatus_v,
            },
        },
        "columns": [
            {"data": "Folio",
                render:function(data,type,row){
                    var html='';
                        if(row.Folio>0){
                            html=row.Folio;
                        }
                    return html;
                }
            },
            {"data": "FacturasId",
            	render:function(data,type,row){
            		var html='';
            		if(row.Estado==1){
                        
				        html='<span class="switch switch_morado switch-icon">\
                                    <label>\
                                        <input type="checkbox" class="facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'">\
                                        <span></span>\
                                    </label>\
                                </span>';
                    }else{
				        html='<span class="switch switch_morado switch-icon">\
                                    <label>\
                                        <input type="checkbox" class="facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" disabled>\
                                        <span></span>\
                                    </label>\
                                </span>';
                    }
                    return html;
            	}
        	},
           
            
            {"data": "Nombre"},
            {"data": "Rfc"},
            {"data": "total",
                render:function(data,type,row){
                    var html='';
                        
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.total);
                    return html;
                }
            },
            {"data": 'fechatimbre'},
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='<a class="btn btn-danger  font-weight-bold btn-pill">Cancelado</a>';
                        html+='<div class="validarcancelacion '+row.uuid+'" data-uuid="'+row.uuid+'" style="color:red"></div>';
                    }else if(row.Estado==1){
                        html='<a class="btn btn-primary font-weight-bold btn-pill">Timbrado</a>';
                    }else if(row.Estado==2){
                        html='<a class="btn btn-warning font-weight-bold btn-pill">Sin timbrar</a>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": 'nombre'},
            {"data": null,
            	render:function(data,type,row){
                    var html='';
                        
                    html='<a href="'+base_url+'/Timbrado/add?duplicado='+row.FacturasId+'" class="btn btn_light_morado font-weight-bold btn-pill">Duplicar</a>';
                    return html;
                }
        	},
            {"data": 'favorito',
            	render:function(data,type,row){
                    var html='';
                        if(row.favorito==1){
                        	var startchecked='checked';
                        }else{
                        	var startchecked='';
                        }
                    html='<input class="star_f" type="checkbox" title="Favorito" id="star_factura_'+row.FacturasId+'" onclick="favorito('+row.FacturasId+')" '+startchecked+'>';
                    return html;
                }
        	},
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<div class="btns-factura">';
                    if(row.Estado==0){ // cancelado
                        html+='<a \
		                        class="btn btn-warning tooltipped" \
		                        href="'+base_url+'Folios/facturapdf/'+row.FacturasId+'" \
		                        target="_blank"\
		                        data-position="top" data-delay="50" data-tooltip="Factura"\
		                      ><i class="icon-xl far fa-file-pdf"></i>\
                      		</a> ';
		                html+='<a\
			                        class="btn btn-warning tooltipped" \
			                        href="'+base_url+row.rutaXml+'" \
			                        target="_blank"\
			                        data-position="top" data-delay="50" data-tooltip="XML"\
			                      download ><i class="icon-xl fa fa-file-code-o"></i>\
                      				</a> ';
                        html+='<a\
                                    class="btn btn-warning tooltipped" \
                                    href="'+base_url+'hulesyg/facturas/'+row.rutaAcuseCancelacion+'" \
                                    target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="Acuse de cancelación"\
                                  download ><i class="icon-xl fas fa-file-invoice" style="color: #ef7e4f;"></i>\
                                    </a> ';
                    }else if(row.Estado==1){
                        html+='<a \
		                        class="btn btn-warning tooltipped" \
		                        href="'+base_url+'Folios/facturapdf/'+row.FacturasId+'" \
		                        target="_blank"\
		                        data-position="top" data-delay="50" data-tooltip="Factura"\
		                      ><i class="icon-xl far fa-file-pdf"></i>\
                      		</a> ';
		                html+='<a\
			                        class="btn btn-primary tooltipped" \
			                        href="'+base_url+row.rutaXml+'" \
			                        target="_blank"\
			                        data-position="top" data-delay="50" data-tooltip="XML"\
			                      download ><i class="icon-xl fa fa-file-code-o"></i>\
                      			</a> ';
                                if(row.FormaPago=='PPD'){
                                    html+='<a\
                                            class="btn btn-warning tooltipped facturacomplemento " \
                                            onclick="complementop('+row.FacturasId+')"\
                                            data-position="top" \
                                            data-facturacomplemeto="'+row.FacturasId+'" \
                                            data-facturatotal="'+row.total+'" \
                                            data-delay="50" data-tooltip="Complemento de pago"\
                                          download ><i class="icon-xl fas fa-receipt facturacomplemento_'+row.FacturasId+'" ></i>\
                                            </a> ';
                                }
                            if(row.correoenviado==1){
                                var stylecorreo='style="color: #12264b;"';
                            }else{
                                var stylecorreo='';
                            }
                        var color_correo='btn-primary';
                        if(row.estatus_correo==1){
                            color_correo='btn-success';
                        }
                        html+='<a type="button"\
                          class="btn '+color_correo+' tooltipped btn-retimbrar" onclick="enviarfactura('+row.FacturasId+','+row.clienteId+')"\
                          data-position="top" data-delay="50" data-tooltip="Editar" id="refacturar">\
                          <i class="icon-xl fa fa-share-alt" '+stylecorreo+'></i>\
                        </a> ';

                        
                    }else if(row.Estado==2){
                        if(row.Folio>0){
                            html+='<a class="btn btn-danger font-weight-bold btn-pill" onclick="retimbrar('+row.FacturasId+',0)">Retimbrar</a>  ';
                        }
                        html+='<a type="button"\
                            href="'+base_url+'Timbrado/add/'+row.FacturasId+'"\
                          class="btn btn-warning tooltipped btn-retimbrar" \
                          data-position="top" data-delay="50" data-tooltip="Editar" id="refacturar">\
                          <i class="icon-xl fas fa-pencil-alt"></i>\
                        </a> ';
                        
                        html+='<a type="button"\
                          class="btn btn-warning tooltipped btn-retimbrar" onclick="deletefactura('+row.FacturasId+')"\
                          data-position="top" data-delay="50" data-tooltip="Editar" id="refacturar">\
                          <i class="icon-xl far fa-trash-alt"></i>\
                        </a> ';
                        
                        
                    }else{
                    	html+='';
                    }
                    html+='</div>';
                    return html;
                }
            }
            
          
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    }).on('draw',function(){
        $(".facturacomplemento").each(function() {
            var factura = parseFloat($(this).data('facturacomplemeto'));
            var total = parseFloat($(this).data('facturatotal'));
            estatusdecomplementos(factura,total);
        });
        verificarcancelacion();

    });
}
function estatusdecomplementos(factura,total){
    $.ajax({
        type:'POST',
        url: base_url+'Folios/estatusdecomplementos',
        data: {
            factura:factura
        },
        statusCode:{
            404: function(data){
                //Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                //Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            var totalcomplemento=parseFloat(data);
            console.log('complemento: '+totalcomplemento);
            console.log('factura: '+total);
            if(totalcomplemento>=total){
                console.log('se completo');
                $(".facturacomplemento_"+factura).css("color","#102243");
            }
           
        }
    });
}
function favorito(id){
	setTimeout(function(){ 
		var statusfav=$('#star_factura_'+id).is(':checked')==true?1:0;
		$.ajax({
	        type:'POST',
	        url: base_url+'Folios/favorito',
	        data: {
	        	factura:id,
	        	status:statusfav
	        },
	        statusCode:{
	            404: function(data){
	                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	            },
	            500: function(){
	                Swal.fire("Error!", "500", "error");
	            }
	        },
	        success:function(data){
	        	
	            Swal.fire({
	                position: "Éxito",
	                icon: "success",
	                title: "Guardado Correctamente",
	                showConfirmButton: false,
	                timer: 1500
	            });
	           
	        }
	    });
    }, 1000);
}
function retimbrar(idfactura,contrato){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de generar el retimbrado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Timbrado/retimbrar",
                    data: {
                        factura:idfactura
                    },
                    success:function(response){  
                        console.log(response);
                        //console.log(array.MensajeErrorDetallado);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            
                            Swal.fire({
				                position: "Éxito",
				                icon: "error",
				                title: "Error "+array.CodigoRespuesta+"!",
				                html: '<span title="'+array.info2+'">'+array.MensajeError+'</span>',
				                //footer: 'Detalle: '+array.info2,
				            });
                        }else{
                            Swal.fire({
				                position: "Éxito",
				                icon: "success",
				                title: "Se ha creado la factura",
				                showConfirmButton: false,
				                timer: 1500
				            });
                            loadtable();
                        }

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
/*
function cancelarfacturas(){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de realizar la Cancelacion de una o mas facturas?<br>Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" autocomplete="off" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    $('body').loading({theme: 'dark',message: 'Cancelando factura...'});
            	//===================================================
                	var facturaslis = $("#tabla_facturacion tbody > tr");
                	var DATAa  = [];
                	var num_facturas_selected=0;
    		        facturaslis.each(function(){  
    		        	if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
    		        		num_facturas_selected++;
    		        		item = {};                    
    		            	item ["FacturasIds"]  = $(this).find("input[class*='facturas_checked']").val();
    		            	DATAa.push(item);
    		        	}       
    		            
    		        });
    		        INFOa  = new FormData();
    		        aInfoa   = JSON.stringify(DATAa);
    		        //console.log(aInfoa);
    		        if (num_facturas_selected==1) {
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Timbrado/cancelarCfdi",
                            data: {
                                facturas:aInfoa
                            },
                            success:function(response){  
                                console.log(response);
                                var array = $.parseJSON(response);
                                $('body').loading('stop');
                                if (array.resultado=='error') {
                                    Swal.fire({
						                position: "Éxito",
						                icon: "error",
						                title: "Error "+array.CodigoRespuesta+"!",
						                text: array.MensajeError,
						                showConfirmButton: false,
						                timer: 2500
						            });
                                }else{
                                    Swal.fire({
						                position: "Éxito",
						                icon: "success",
						                title: "Se ha cancelado la factura",
						                showConfirmButton: false,
						                timer: 1500
						            });
                                    loadtable();
                                }

                            }
                        });
    		        }else{
    		        	Swal.fire({
						                position: "Éxito",
						                icon: "warning",
						                title: "Atención!",
						                text: 'Seleccione solo una factura',
						                showConfirmButton: false,
						                timer: 2500
						            });
    		        }
                //================================================
                                }else{
                                    Swal.fire({
						                position: "Éxito",
						                icon: "warning",
						                title: "Atención!",
						                text: 'No tiene permiso',
						                showConfirmButton: false,
						                timer: 2500
						            });
                                }
                        },
                        error: function(response){
                           
                            Swal.fire({
						                position: "Éxito",
						                icon: "error",
						                title: "Error!",
						                text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',
						                showConfirmButton: false,
						                timer: 2500
						            });  
                             
                        }
                    });
                    
                }else{
                    
                    Swal.fire({
						                position: "Éxito",
						                icon: "warning",
						                title: "Advertencia!",
						                text: 'Ingrese una contraseña',
						                showConfirmButton: false,
						                timer: 2500
						            });  
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
*/
function cancelarfacturas(){
    var html='<div class="row col-md-12">\
                <div class="col-md-12">\
                    ¿Está seguro de realizar la Cancelacion de la factura?<br>Se necesita permisos de administrador\
                </div>\
                <div class="col-md-12">\
                    <form method="post" action="#" autocomplete="off">\
                    <input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>\
                    </form>\
                </div>\
              </div>\
              <div class="row col-md-12">\
                <div class="col-md-6">\
                    <label>Motivo Cancelación</label>\
                    <select id="motivocancelacion" class="form-control" onchange="mocance()">\
                        <option value="00" disabled selected> Seleccione una opción</option>\
                        <option value="01" >01 Comprobante emitido con errores con relación</option>\
                        <option value="02" >02 Comprobante emitido con errores sin relación</option>\
                        <option value="03" >03 No se llevó a cabo la operación</option>\
                        <option value="04" >04 Operación nominativa relacionada en una factura global</option>\
                    </select>\
                </div>\
                <div class="col-md-6 class_foliorelacionado" style="display:none">\
                    <label>Folio Relacionado</label>\
                    <input type="text" id="foliorelacionado" class="form-control" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">\
                </div>\
              </div>';
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                var motivo = $('#motivocancelacion option:selected').val();
                var uuidrelacionado=$('#foliorelacionado').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                //===================================================
                    var facturaslis = $("#tabla_facturacion tbody > tr");
                    var DATAa  = [];
                    var num_facturas_selected=0;
                    facturaslis.each(function(){  
                        if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
                            num_facturas_selected++;
                            item = {};                    
                            item ["FacturasIds"]  = $(this).find("input[class*='facturas_checked']").val();
                            
                            DATAa.push(item);
                        }       
                        
                    });
                    INFOa  = new FormData();
                    aInfoa   = JSON.stringify(DATAa);
                    //console.log(aInfoa);
                    if (num_facturas_selected==1) {
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Timbrado/cancelarCfdi",
                            data: {
                                facturas:aInfoa,
                                motivo:motivo,
                                uuidrelacionado:uuidrelacionado
                            },
                            success:function(response){  
                                console.log(response);
                                var array = $.parseJSON(response);
                                if (array.resultado=='error') {
                                    Swal.fire({
                                        position: "Éxito", icon: "error",
                                        title: "Error "+array.CodigoRespuesta+"!",
                                        text: array.MensajeError,
                                        showConfirmButton: false, timer: 2500
                                    });
                                }else{
                                    Swal.fire({
                                        position: "Éxito",icon: "success",title: "Se ha cancelado la factura",showConfirmButton: false,timer: 1500
                                    });
                                    loadtable();
                                }

                            }
                        });
                    }else{
                        Swal.fire({
                                position: "Éxito",icon: "warning",title: "Atención!",text: 'Seleccione solo una factura',showConfirmButton: false,timer: 2500
                                    });
                    }
                //================================================
                                }else{
                                    Swal.fire({
                                        position: "Éxito",icon: "warning",title: "Atención!",text: 'No tiene permiso',showConfirmButton: false,timer: 2500
                                    });
                                }
                        },
                        error: function(response){
                            Swal.fire({
                                        position: "Éxito",icon: "error",title: "Error!",text: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema',showConfirmButton: false,timer: 2500
                                    });    
                             
                        }
                    });
                    
                }else{
                    Swal.fire({
                            position: "Éxito",icon: "warning",title: "Advertencia!",text: 'Ingrese una contraseña',showConfirmButton: false,timer: 2500
                                    });  
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function mocance(){
    var motivo=$('#motivocancelacion option:selected').val();
    if(motivo=='01'){
        $('.class_foliorelacionado').show('show');
    }else{
        $('.class_foliorelacionado').hide('show');
        $('#foliorelacionado').val('');
    }
}
var facturacomplemtoid;
function complementop(facturacomplemto){
    facturacomplemtoid=facturacomplemto;
    $('#modalcomplementos').modal();
    $('#modalcomplementos').modal('show');
    $('.listadocomplementos').html('');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Folios/listasdecomplementos",
        data: {
            facturas:facturacomplemto
        },
        success:function(response){  
           $('.listadocomplementos').html(response);
           $('#tableliscomplementos').DataTable();
        }
    });
}
function addcomplemento(){
    window.location.href = base_url+'complementosPago/add/'+facturacomplemtoid;
}

function enviarfactura(factura,cliente){
    $('.iframepdf').html('<iframe src="'+base_url+'Folios/facturapdf/'+factura+'"></iframe>');
    envionfactura=factura;
    enviocliente=cliente;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Folios/correoscliente",
        data: {
            cliente:cliente
        },
        success:function(response){  
           $('.enviocorreo').html(response);
        }
    });
    $('#modalseleccioncorreo').modal('show');
}
function search(){
    var searchtext = $('#searchtext').val();
    table.search(searchtext).draw();
}
function deletefactura(id){
    

    $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Desea eliminar este registro, recuerda que no se encuentra timbrado?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    //======================================================
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Folios/deletefactura",
                        data: {
                            factura:id
                        },
                        success:function(response){  
                           loadtable();
                        }
                    });
                    //=========================================
                },
                cancelar: function () 
                {
                    
                }
            }
        });
}
function deletemailcli(row){
    $('.climailid_'+row).remove();
}
function verificarcancelacion() {
    
    var DATAvc  = [];
    $(".validarcancelacion").each(function() {
        item = {};
        //item ["idfactura"] = $(this).data('uuid');
        item ["uuid"] = $(this).data('uuid');
        DATAvc.push(item);
    });
    console.log(DATAvc);
    aInfovc   = JSON.stringify(DATAvc);
    console.log(aInfovc);
    console.log('Cancelados:'+aInfovc.length);
    var datos=aInfovc;
    
    $.ajax({
        type:'POST',
        url: base_url+"Timbrado/estatuscancelacionCfdi_all",
        data: {
            datos:aInfovc
        },
        success: function (data){
            console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                
                    $("."+item.uuid ).html(item.resultado);
                
            });
        }
    });
}