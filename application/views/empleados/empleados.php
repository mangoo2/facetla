<input type="hidden" id="perfil" value="<?php echo $perfil ?>">
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php if($perfil!=2){ ?>
                                        <button type="button" class="btn btn-outline-info"  onclick="modal_empleado()"><i class="fas fa-user-plus"></i> Nuevo Empleado</button> 
                                        <?php } ?>
                                        <!--
                                        <button type="button" class="btn btn-outline-info" disabled><i class="fas fa-paper-plane"></i> Enviar cita y formulario</button> 
                                        <button type="button" class="btn btn-outline-info" disabled><i class="far fa-heart"></i> Recomentar</button>
                                    -->
                                    </div>
                                    <div class="col-md-6">    
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text">Buscar Empleado &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="empleado_busqueda" type="text" class="form-control" placeholder="Escriba nombre del empleado" oninput="reload_registro()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12"></div>
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>ID</th> 
                                                    <th>Nombre del empleado</th>
                                                    <th>Puesto</th>
                                                    <th>Celular</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>    
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>            

<div id="registro_empleado" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                <h4 class="modal-title">Registro de empleado</h4>
                </div>
                <div class="col-lg-1">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" id="foto_validar" value="0">
                                <form class="form" method="post" role="form" id="form_empleado">
                                    <input type="hidden" name="personalId" id="personalId" value="0"> 
                                    
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="foto_empl">
                                                <img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="<?php echo base_url(); ?>images/annon.png">    
                                            </div>
                                            <span class="foto_avatar" >
                                            <input type="file" id="foto_avatar" style="display: none">
                                            </span>
                                            <label for="foto_avatar">
                                                    <span class="btn waves-effect waves-light btn-secondary"><i class="fas fa-camera"></i> Cambiar foto</span>
                                            </label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label><span style="color: red;">*</span> Nombre completo</label>
                                                        <input type="text" name="nombre" id="nombre" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>Correo Electrónico</label>
                                                        <input type="email" name="correo" id="correo" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Celular</label>
                                                        <input type="text" name="celular" id="celular" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Puesto</label>
                                                        <input type="text" name="puesto" id="puesto" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Perímetro</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="color1" name="color" class="custom-control-input" value="red">
                                                        <label class="custom-control-label" for="color1" style="background-color: red;color: red;">____</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="color2" name="color" class="custom-control-input" value="orange">
                                                        <label class="custom-control-label" for="color2" style="background-color: orange;color: orange;">____</label>
                                                    </div>
                                                </div>  
                                                <div class="col-sm-2">  
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="color3" name="color" class="custom-control-input" value="yellow">
                                                        <label class="custom-control-label" for="color3" style="background-color: yellow;color: yellow;">____</label>
                                                    </div>
                                                </div>    
                                                <div class="col-sm-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="color4" name="color" class="custom-control-input" value="#03a9f4">
                                                        <label class="custom-control-label" for="color4" style="background-color: #03a9f4;color: #03a9f4;">____</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label class="custom-control custom-checkbox m-b-0">
                                                    <input type="checkbox" class="custom-control-input" name="check_baja" id="verificar_check" onclick="check_baja_btn()">
                                                    <span class="custom-control-label" style="color: red">Dar de baja al empleado</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="baja_texto" style="display: none">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Fecha de baja</label>
                                                    <input type="date" name="fechabaja" id="fechabaja" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Motivo</label>
                                                    <textarea name="motivo" id="motivo" class="form-control js-auto-size"></textarea>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>    
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary waves-effect btn_registro" onclick="guarda_empleado()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="elimina_empleado_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar éste empleado? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_empleado">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_empleado()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>

<div id="usuario_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Usuario</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="text_usuario"></div>
            </div>
            <input type="hidden" id="id_empleado">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn_sistema waves-effect btn_registro" onclick="guardar_usuario()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>
<div class="iframe_datos" style="display: none;">     
</div>
