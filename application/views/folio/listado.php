<style type="text/css">
    .star {
        visibility:hidden;
        font-size:30px;
        cursor:pointer;
        margin-top: 15px;
    }
    .star:before {
       content: "\2605";
       position: absolute;
       visibility:visible;
    }
    .star:checked:before {
       content: "\2605";
       position: absolute;
       color: #ffeb3b;
    }
    .star_f {
        visibility:hidden;
        font-size:30px;
        cursor:pointer;
        margin-top: 5px;
    }
    .star_f:before {
       content: "\2605";
       position: absolute;
       visibility:visible;
       margin-top: -19px;
    }
    .star_f:checked:before {
       content: "\2605";
       position: absolute;
       color: #ffeb3b;
       margin-top: -19px;
    }
    .dataTables_filter label, .dataTables_paginate .pagination{
        float: right;
    }
    .btns-factura{
        min-width: 180px;
    }
    .btn-retimbrar span i:active{
        color:#12264b;
    }
    #tabla_facturacion_filter{
        display: none;
    }
    #tabla_facturacion td{
        padding-left: 9px !important;
        padding-right: 9px !important;
        font-size: 12px;
    }
    @media (max-width:550px){
        .container{
            padding: 0 5px;
        }
        .card-body{
            padding: 2rem 2rem;;
        }
        td{
            padding-left: 5px !important;
            padding-right: 5px !important;
            font-size: 10px;
        }
    }
</style>
<div  clas="row" style="display: none;">
 <input type="password" name="" >
  <input type="search" name="" > 
</div>
<!--begin::Content-->
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col">
                    <div class="mb-3 row">
                        <div class="col-sm-4"> 
                            <div class="mb-3">
                                <input class="form-control" type="search" id="searchtext" placeholder="Buscar factura" oninput="search()"> 
                            </div>     
                        </div> 
                        <div class="col-sm-2"></div>
                        <div class="col-sm-3">
                            <div class="mb-3">
                                <a href="<?php echo base_url() ?>Timbrado" class="btn btn-primary btn-block">Nueva factura</a>
                            </div>    
                        </div>
                        <div class="col-sm-3">
                            <div class="mb-3">
                                <a class="btn btn-danger font-weight-bold btn-block" onclick="cancelarfacturas()">Cancelar factura</a>
                            </div>    
                        </div>
                    </div>
                </div>        
            </div>
            <div class="row">
                <div class="col">
                    <div class="mb-3 row"> 
                        <div class="col-sm-3"> 
                            <div class="mb-3">
                                <label class="form-label">Seleccionar estatus</label>
                                <select class="form-control" id="estatus_v" onchange="loadtable()">
                                    <option value="0">Todas</option>
                                    <option value="1">Timbradas</option>
                                    <option value="2">Guardadas</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4"> 
                            <div class="mb-3">
                                <label class="form-label">Seleccionar cliente</label>
                                <select class="form-control" id="idcliente" onchange="loadtable()">
                                    <?php if(isset($_GET['id'])){ ?>
                                        <option value="<?php echo $_GET['id'];?>"><?php echo $_GET['name'];?></option>    
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">                 
                            <div class="mb-3">
                                <label class="form-label">Fecha inicio</label>
                                <input type="date" class="form-control" id="finicial" onchange="loadtable()">
                            </div>
                        </div>
                        <div class="col-sm-1">                     
                            <div class="mb-3">
                                <input class="star form-control" type="checkbox" title="bookmark page" id="facturas_star" onchange="loadtable()">
                            </div>
                        </div>
                    </div>        
                </div>    
            </div>



            

              
            <!--begin::Dashboard-->
            <div class="card">
                <div class="card-body">
                    <div class="row">   
                        <div class="col">
                            <div class="mb-3 row">  
                                <div class="col-sm-12" style="padding: 0px;">
                                    <table class="table table-sm" id="tabla_facturacion">
                                        <thead>
                                            <tr><th>Folio</th>
                                                <th></th>
                                                
                                                <th>Nombre / Razón social</th>
                                                <th>RFC</th>
                                                <th>Monto</th>
                                                <th>Fecha</th>
                                                <th>Estatus</th>
                                                <th>Emitió</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>        
                    </div>
                    <div class="row">    
                        <div class="col-md-12 iframepdf" style="display: none;">
                            
                        </div>
                    </div>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<div class="modal fade" id="modalcomplementos" tabindex="-1" aria-labelledby="exampleModalSizeXl" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Documentos del cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="col-md-12">
                    <a class="btn btn-primary" onclick="addcomplemento()" style="background: #12264b;color: white;">Nuevo</a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 listadocomplementos">
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal" style="background: #fcb520;color: white;">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalseleccioncorreo" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #54291b;"><b>Correos a enviar</b></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" id="table_mail_envios">
                            <thead>
                                <tr>
                                    <th>Correo</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="enviocorreo">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning font-weight-bold" data-dismiss="modal" style="background: #fcb520;color: white;" onclick="$('#modalseleccioncorreo').modal('hide')">Cancelar</button>
                <button type="button" class="btn btn-primary font-weight-bold enviocorreomail" style="background: #12264b;color: white;">Enviar</button>
            </div>
        </div>
    </div>
</div>