<style type="text/css">
    .dt-buttons .dt-button {
        padding: 5px 15px;
        border-radius: .25rem;
        background: #026934 !important;
        color: #fff;
        margin-right: 3px;
    }
</style>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Registro de check points</h3>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Fecha inicio</label>
                                            <input type="date" id="fecha_inicio" class="form-control" onchange="fechas_in_fi()">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Fecha fin</label>
                                            <input type="date" id="fecha_fin" class="form-control" onchange="fechas_in_fi()">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <label>Fecha actual</label>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="hidden" id="fecha_hoy" value="<?php echo $fecha_hoy ?>">
                                                <input type="checkbox" class="custom-control-input" id="fecha_actual" value="check" onclick="fecha_actual()">
                                                <label class="custom-control-label" for="fecha_actual"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Seleccionar check points</label>
                                            <select id="points_check" class="form-control" onchange="fechas_in_fi()">
                                                <option value="0" selected="">Todos</option>
                                                <?php $result=$this->General_model->getselectwhere('check_points','activo',1);
                                                foreach ($result as $item){
                                                    echo '<option value="'.$item->id.'">'.$item->nombre.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12"></div>
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>ID</th> 
                                                    <th>Fecha y hora</th>
                                                    <th>Empleado</th>
                                                    <th>Check points</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>   