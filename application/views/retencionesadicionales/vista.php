
<div class="page-body">
          <!-- Container-fluid starts-->
          <div class="container-fluid dashboard-default-sec">
            <div class="row">
                <div class="col-lg-10" align="right"> 
                </div>   
                <div class="col-lg-2" align="right"> 
                    <a href="<?php echo base_url() ?>Inicio" class="btn btn-primary">Regresar</a>
                </div>   

            </div>  
            <br> 
            <!--begin::Dashboard-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <form>
                        <div class="card-body">
                            <?php foreach ($result->result() as $item) { 
                                if($item->estatus==1){
                                    $checkedestatus='checked';
                                }else{
                                    $checkedestatus='';
                                }
                            ?>
                                <div class="form-group row">
                                    <h3 class="col-5"><?php echo $item->retencion;?></h3>
                                    <div class="col-3">
                                        <span class="switch switch_morado switch-icon">
                                            <label>
                                                <input 
                                                    type="checkbox" 
                                                    id="retencion_<?php echo $item->retencionId;?>" 
                                                    onclick="updatestatus(<?php echo $item->retencionId;?>)"
                                                    <?php echo $checkedestatus;?> >
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>
                                </div>        
                            <?php  } ?>
                            
                        </div>
                    </form>
                </div>                
            </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>