<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloReportes extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function reporte_prods($params){
        $this->db->select("csvd.idconsulta, csvd.total, csvd.preciou, cs.consultafecha, pa.lote, p.producto, m.nombre as marca, t.nombre as tipo, 1 as tipo_tabla,
            pe.nombre as personal");
        $this->db->from("consulta_spa cs");
        $this->db->join("consulta_spa_productos_venta_detalles csvd","csvd.idconsulta=cs.idconsulta");
        $this->db->join("productos_almacen pa","pa.idalmacen=csvd.idalmacen");
        $this->db->join("productos p","p.idproducto=pa.idproducto");
        //$this->db->join("categoria c","c.idcategoria=p.categoria");
        $this->db->join("tipo t","t.idtipo=p.tipo");
        $this->db->join("marca m","m.idmarca=p.marca");
        $this->db->join("personal pe","pe.personalId=cs.personalId","left");

        $this->db->where("cs.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->where("cs.activo",1);
        $this->db->where("csvd.activo",1);
        $this->db->group_by("csvd.iddetalles");
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("pvd.idconsulta, pvd.total, pvd.preciou, cme.consultafecha, pa.lote, p.producto, m.nombre as marca, t.nombre as tipo, 2 as tipo_tabla, pe.nombre as personal");
        $this->db->from("consulta_medicina_estetica cme");
        $this->db->join("productos_venta_detalles pvd","pvd.idconsulta=cme.idconsulta");
        $this->db->join("productos_almacen pa","pa.idalmacen=pvd.idalmacen");
        $this->db->join("productos p","p.idproducto=pa.idproducto");
        //$this->db->join("categoria c","c.idcategoria=p.categoria");
        $this->db->join("tipo t","t.idtipo=p.tipo");
        $this->db->join("marca m","m.idmarca=p.marca");
        $this->db->join("personal pe","pe.personalId=cme.personalId","left");
        $this->db->where("cme.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->where("cme.activo",1);
        $this->db->where("pvd.activo",1);
        $this->db->group_by("pvd.iddetalles");
        $query2 = $this->db->get_compiled_select();

        $this->db->select("cnpvd.idconsulta, cnpvd.total, cnpvd.preciou, cn.consultafecha, pa.lote, p.producto, m.nombre as marca, t.nombre as tipo, 3 as tipo_tabla, pe.nombre as personal");
        $this->db->from("consulta_nutricion cn");
        $this->db->join("consulta_nutricion_productos_venta_detalles cnpvd","cnpvd.idconsulta=cn.idconsulta");
        $this->db->join("productos_almacen pa","pa.idalmacen=cnpvd.idalmacen");
        $this->db->join("productos p","p.idproducto=pa.idproducto");
        //$this->db->join("categoria c","c.idcategoria=p.categoria");
        $this->db->join("tipo t","t.idtipo=p.tipo");
        $this->db->join("marca m","m.idmarca=p.marca");
        $this->db->join("personal pe","pe.personalId=cn.personalId","left");
        $this->db->where("cn.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->where("cn.activo",1);
        $this->db->where("cnpvd.activo",1);
        $this->db->group_by("cnpvd.iddetalles");
        $query3 = $this->db->get_compiled_select();

        $this->db->select("ppvd.idventa, ppvd.total, ppvd.preciou, ppv.reg as consultafecha, pa.lote, p.producto, m.nombre as marca, t.nombre as tipo, 4 as tipo_tabla, pe.nombre as personal");
        $this->db->from("pacientes_productos_venta ppv");
        $this->db->join("pacientes_productos_venta_detalles ppvd","ppvd.idventa=ppv.idventa");
        $this->db->join("productos_almacen pa","pa.idalmacen=ppvd.idalmacen");
        $this->db->join("productos p","p.idproducto=pa.idproducto");
        $this->db->join("tipo t","t.idtipo=p.tipo");
        $this->db->join("marca m","m.idmarca=p.marca");
        $this->db->join("personal pe","pe.personalId=ppv.personalId","left");

        $this->db->where("ppv.reg between '{$params['fechai']} 00:00:00' AND '{$params['fechaf']} 23:59:59' ");
        $this->db->where("ppv.activo",1);
        $this->db->where("ppvd.activo",1);
        $this->db->group_by("ppvd.iddetalles");
        $query4 = $this->db->get_compiled_select();

        return $this->db->query($query1." UNION ".$query2." UNION ".$query3." UNION ".$query4)->result();
    }

    public function reporte_total_consult($params){
        $this->db->select("count(distinct csvd.idconsulta) as totConsul, 1 as tipo_tabla");
        $this->db->from("consulta_spa cs");
        $this->db->join("consulta_spa_productos_venta_detalles csvd","csvd.idconsulta=cs.idconsulta");
        $this->db->where("cs.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->join("productos_almacen pa","pa.idalmacen=csvd.idalmacen");
        $this->db->join("productos p","p.idproducto=pa.idproducto");
        $this->db->join("tipo t","t.idtipo=p.tipo");
        $this->db->join("marca m","m.idmarca=p.marca");
        $this->db->where("cs.activo",1);
        $this->db->where("csvd.activo",1);

        if($params["texto"]!=""){
            $this->db->like("p.producto",$params["texto"]);
            $this->db->or_like("pa.lote",$params["texto"]);
            $this->db->or_like("cs.consultafecha",$params["texto"]);
            $this->db->or_like("t.nombre",$params["texto"]);
            $this->db->or_like("m.nombre",$params["texto"]);
        }
        $query1 = $this->db->get_compiled_select(); 

        $this->db->select("count(distinct  pvd.idconsulta) as totConsul, 2 as tipo_tabla");
        $this->db->from("consulta_medicina_estetica cme");
        $this->db->join("productos_venta_detalles pvd","pvd.idconsulta=cme.idconsulta");
        $this->db->join("productos_almacen pa","pa.idalmacen=pvd.idalmacen");
        $this->db->join("productos p","p.idproducto=pa.idproducto");
        $this->db->join("tipo t","t.idtipo=p.tipo");
        $this->db->join("marca m","m.idmarca=p.marca");
        $this->db->where("cme.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->where("cme.activo",1);
        $this->db->where("pvd.activo",1);
        if($params["texto"]!=""){
            $this->db->like("p.producto",$params["texto"]);
            $this->db->or_like("pa.lote",$params["texto"]);
            $this->db->or_like("cme.consultafecha",$params["texto"]);
            $this->db->or_like("t.nombre",$params["texto"]);
            $this->db->or_like("m.nombre",$params["texto"]);
        }
        $query2 = $this->db->get_compiled_select();

        $this->db->select("count(distinct  cnpvd.idconsulta) as totConsul, 3 as tipo_tabla");
        $this->db->from("consulta_nutricion cn");
        $this->db->join("consulta_nutricion_productos_venta_detalles cnpvd","cnpvd.idconsulta=cn.idconsulta");
        $this->db->join("productos_almacen pa","pa.idalmacen=cnpvd.idalmacen");
        $this->db->join("productos p","p.idproducto=pa.idproducto");
        $this->db->join("tipo t","t.idtipo=p.tipo");
        $this->db->join("marca m","m.idmarca=p.marca");
        $this->db->where("cn.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->where("cn.activo",1);
        $this->db->where("cnpvd.activo",1);
        if($params["texto"]!=""){
            $this->db->like("p.producto",$params["texto"]);
            $this->db->or_like("pa.lote",$params["texto"]);
            $this->db->or_like("cn.consultafecha",$params["texto"]);
            $this->db->or_like("t.nombre",$params["texto"]);
            $this->db->or_like("m.nombre",$params["texto"]);
        }
        $query3 = $this->db->get_compiled_select();

        return $this->db->query($query1." UNION ".$query2." UNION ".$query3)->result();
    }

    public function reporte_spa($params){
        $this->db->select("cssv.idconsulta, cssv.costo, cs.consultafecha, s.nombre as servicio, concat(p.nombre,' ',p.apll_paterno,' ',p.apll_materno) as paciente, pe.nombre as personal");
        $this->db->from("consulta_spa cs");
        $this->db->join("consulta_spa_servicio_venta_detalles cssv","cssv.idconsulta=cs.idconsulta");
        $this->db->join("pacientes p","p.idpaciente=cs.idpaciente");
        $this->db->join("servicios s","s.idservicio=cssv.idservicio");
        $this->db->join("personal pe","pe.personalId=cs.personalId","left");

        $this->db->where("cs.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->where("cs.activo",1);
        $this->db->where("cssv.activo",1);        
        $query=$this->db->get();
        return $query->result();
    }

    public function reporte_med($params){
        $this->db->select("cme.idconsulta, svd.costo, cme.consultafecha, s.nombre as servicio, concat(p.nombre,' ',p.apll_paterno,' ',p.apll_materno) as paciente, pe.nombre as personal");
        $this->db->from("consulta_medicina_estetica cme");
        $this->db->join("servicio_venta_detalles svd","svd.idconsulta=cme.idconsulta");
        $this->db->join("pacientes p","p.idpaciente=cme.idpaciente");
        $this->db->join("servicios s","s.idservicio=svd.idservicio");
        $this->db->join("personal pe","pe.personalId=cme.personalId","left");
        $this->db->where("cme.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->where("cme.activo",1);
        $this->db->where("svd.activo",1);        
        $query=$this->db->get();
        return $query->result();
    }

    public function reporte_nutri($params){ 
        $this->db->select("cn.idconsulta, cnsvd.costo, cn.consultafecha, s.nombre as servicio, concat(p.nombre,' ',p.apll_paterno,' ',p.apll_materno) as paciente, pe.nombre as personal");
        $this->db->from("consulta_nutricion cn");
        $this->db->join("consulta_nutricion_servicio_venta_detalles cnsvd","cnsvd.idconsulta=cn.idconsulta");
        $this->db->join("pacientes p","p.idpaciente=cn.idpaciente");
        $this->db->join("servicios s","s.idservicio=cnsvd.idservicio");
        $this->db->join("personal pe","pe.personalId=cn.personalId","left");
        $this->db->where("cn.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->where("cn.activo",1);
        $this->db->where("cnsvd.activo",1);
      
        $query=$this->db->get();
        return $query->result();
    }

    public function reporte_total_consultSpa($params){
        $this->db->select("count(distinct cssv.idconsulta) as totConsul, 1 as tipo_tabla");
        $this->db->from("consulta_spa cs");
        $this->db->join("consulta_spa_servicio_venta_detalles cssv","cssv.idconsulta=cs.idconsulta");
        $this->db->join("pacientes p","p.idpaciente=cs.idpaciente");
        $this->db->join("servicios s","s.idservicio=cssv.idservicio");
        $this->db->where("cs.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->where("cs.activo",1);
        $this->db->where("cssv.activo",1);

        if($params["texto"]!=""){
            $this->db->like("s.nombre",$params["texto"]);
            $this->db->or_like("p.nombre",$params["texto"]);
            $this->db->or_like("p.apll_paterno",$params["texto"]);
            $this->db->or_like("p.apll_materno",$params["texto"]);
            $this->db->or_like("cs.consultafecha",$params["texto"]);
        }
        
        $query=$this->db->get();
        return $query->result();
    }

    public function reporte_total_consultME($params){
        $this->db->select("count(distinct  svd.idconsulta) as totConsul, 2 as tipo_tabla");
        $this->db->from("consulta_medicina_estetica cme");
        $this->db->join("servicio_venta_detalles svd","svd.idconsulta=cme.idconsulta");
        $this->db->join("pacientes p","p.idpaciente=cme.idpaciente");
        $this->db->join("servicios s","s.idservicio=svd.idservicio");
        $this->db->where("cme.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->where("cme.activo",1);
        $this->db->where("svd.activo",1);

        if($params["texto"]!=""){
            $this->db->like("s.nombre",$params["texto"]);
            $this->db->or_like("p.nombre",$params["texto"]);
            $this->db->or_like("p.apll_paterno",$params["texto"]);
            $this->db->or_like("p.apll_materno",$params["texto"]);
            $this->db->or_like("cme.consultafecha",$params["texto"]);
        }

        $query=$this->db->get();
        return $query->result();
    }

    public function reporte_total_consultNut($params){
        $this->db->select("count(distinct  cnsvd.idconsulta) as totConsul, 3 as tipo_tabla");
        $this->db->from("consulta_nutricion cn");
        $this->db->join("consulta_nutricion_servicio_venta_detalles cnsvd","cnsvd.idconsulta=cn.idconsulta");
        $this->db->join("pacientes p","p.idpaciente=cn.idpaciente");
        $this->db->join("servicios s","s.idservicio=cnsvd.idservicio");

        $this->db->where("cn.consultafecha between '{$params['fechai']}' AND '{$params['fechaf']}' ");
        $this->db->where("cn.activo",1);
        $this->db->where("cnsvd.activo",1);
        if($params["texto"]!=""){
            $this->db->like("s.nombre",$params["texto"]);
            $this->db->or_like("p.nombre",$params["texto"]);
            $this->db->or_like("p.apll_paterno",$params["texto"]);
            $this->db->or_like("p.apll_materno",$params["texto"]);
            $this->db->or_like("cn.consultafecha",$params["texto"]);
        }
        $query=$this->db->get();
        return $query->result();
    }

}