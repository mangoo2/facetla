<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloComplementos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getcomplementos($params){
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $columns = array( 
            0=>'complementoId',
            1=>'Folio',
            2=>'R_nombre',
            3=>'R_rfc',
            4=>'Monto',
            5=>'Estado',
            6=>'fechatimbre',
            7=>'rutaXml',
            8=>'rutaAcuseCancelacion',
            9=>'correoenviado'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('f_complementopago');

        if($finicio!=''){
            $this->db->where(array('fechatimbre >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('fechatimbre <='=>$ffin.' 23:59:59'));
        }
        $this->db->where(array('activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_complementos($params){
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $columns = array( 
            0=>'complementoId',
            1=>'Folio',
            2=>'R_nombre',
            3=>'R_rfc',
            4=>'Monto',
            5=>'Estado',
            6=>'fechatimbre',
            7=>'rutaXml',
            8=>'rutaAcuseCancelacion'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('f_complementopago');
        
        if($finicio!=''){
            $this->db->where(array('fechatimbre >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('fechatimbre <='=>$ffin.' 23:59:59'));
        }
        $this->db->where(array('activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function saldocomplemento($factura){
        $strq = "SELECT doc.ImpPagado
                FROM f_complementopago_documento as doc 
                INNER JOIN f_complementopago as com on com.complementoId=doc.complementoId 
                WHERE com.Estado=1 AND doc.facturasId=$factura
                "; 
        $datoscop = $this->db->query($strq);
        $saldo=0;
        $numcomplem=0;
        foreach ($datoscop->result() as $item) {
            //log_message('error', 'validar saldo0: '.$item->ImpPagado);
            $saldo=$saldo+$item->ImpPagado;
            $numcomplem++;
        }
        //log_message('error', 'validar saldo1: '.$saldo);
        return $saldo; 
    }
    function saldocomplementonum($factura){
        $strq = "SELECT doc.ImpPagado
                FROM f_complementopago_documento as doc 
                INNER JOIN f_complementopago as com on com.complementoId=doc.complementoId 
                WHERE com.Estado=1 AND doc.facturasId=$factura
                "; 
        $datoscop = $this->db->query($strq);
        $saldo=0;
        $numcomplem=0;
        foreach ($datoscop->result() as $item) {
            $saldo=$saldo+$item->ImpPagado;
            $numcomplem++;
        }
        return $numcomplem; 
    }




}