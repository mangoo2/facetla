<?php
class General_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_table($table){
    	$sql = "SELECT * FROM $table";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_select($tables,$where){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }
    public function get_table_active($table){
    	$sql = "SELECT * FROM $table WHERE estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_record($col,$id,$table){
    	$sql = "SELECT * FROM $table WHERE $col=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_records_condition($condition,$table){
    	$sql = "SELECT * FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function add_record($table,$data){
    	$this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function edit_record($cos,$id,$data,$table){
    	$this->db->set($data);
        $this->db->where($cos, $id);
        return $this->db->update($table);
    }
    public function edit_recordw($where,$data,$table){
        $this->db->set($data);
        $this->db->where($where);
        return $this->db->update($table);
    }

    public function delete_records($condition,$table){
    	$sql = "DELETE FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query;
    }
    public function getselectwhere($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwhereall($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwhereall2($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }
    public function getselectlike($tables,$values,$search){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);
        $this->db->or_like($search);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwhere_orden_asc($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'ASC');
        $query=$this->db->get();
        return $query->result();
    }
    public function getselectwhere_orden_desc($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'DESC');
        $query=$this->db->get();
        return $query->result();
    } 
    public function horacioslaborales($dia){
        switch ($dia) {
            case 1:
                $dial='l';
                break;
            case 2:
                $dial='m';
                break;
            case 3:
                $dial='mi';
                break;
            case 4:
                $dial='j';
                break;
            case 5:
                $dial='v';
                break;
            case 6:
                $dial='s';
                break;
            case 7:
                $dial='d';
                break;
        }
        $sql = "SELECT hl.horafin AS horas
                FROM config_horario_laboral AS hl
                WHERE hl.status =1 AND hl.id =$dia
                UNION
                SELECT hl.horainicio AS horas
                FROM config_horario_laboral AS hl
                WHERE hl.status =1 AND hl.id =$dia
                UNION
                SELECT h_inicio AS horas
                FROM config_horario_nodisponible AS hnl
                WHERE hnl.activo =1
                AND hnl.$dial =1
                UNION
                SELECT h_fin AS horas
                FROM config_horario_nodisponible AS hnl
                WHERE hnl.activo =1
                AND hnl.$dial =1
                ORDER BY `horas` ASC";
        $query = $this->db->query($sql);
        return $query;
    }
    public function get_info_random($table, $cantidad){
        $sql = "SELECT * FROM $table WHERE activo=1 ORDER BY RAND() LIMIT $cantidad";
        $query = $this->db->query($sql);
        return $query;
    }

    public function delete_detalle_perfil($id){
        $sql = "DELETE FROM perfiles_detalles WHERE Perfil_detalleId=".$id;
        $query = $this->db->query($sql);
    }
    /*
    public function get_records_menu($id){
        $sql = "SELECT m.Pagina FROM config_general AS c
                INNER JOIN menu_sub AS m ON m.MenusubId = c.MenusubId
                WHERE c.perfilId=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    */
    public function getseleclike($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE activo=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function ultimafechadesession(){
        $strq = "SELECT max(reg) as fecha FROM `historial_session` WHERE activo=1";
        $query = $this->db->query($strq);
        $fecha='';
        foreach ($query->result() as $row) {
            $fecha=$row->fecha;
        }
        return $fecha; 
    }
}