<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->perfilid=$this->session->userdata('perfilid');
  }
	public function index(){
    $id_pefil=$this->perfilid;
    $id_pefil_aux=intval($id_pefil);
    //var_dump($id_pefil_aux);die;
    /*
    $id_pefil_aux=0;
    if($id_pefil!=0){
      $id_pefil_aux=$this->perfilid;
    }else{
      $id_pefil_aux=0; 
    }
    */
    /*
    $resconfig=$this->General_model->get_records_menu($id_pefil_aux);
    $pagina='Empleados';
    foreach ($resconfig as $item) {
      $pagina=$item->Pagina;
    }
    */
    /*
    if($id_pefil_aux==2){
      $pagina='Operacion';
    }else{
      $pagina='Empleados';
    }
    */
    $pagina='Inicio';
		if (!isset($_SESSION['perfilid'])) {
          $perfilview=0;
          redirect('/Login');
        }else{
            $perfilview=$_SESSION['perfilid'];
        }
       	if ($perfilview>=1) {
        	redirect('/'.$pagina);
        }else{
        	redirect('/Login');
        }
	}
  function solicitarpermiso(){
        $pass = $this->input->post('pass');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            
            $verificar = password_verify($pass,$item->contrasena);
            if ($verificar) {
                $permiso=1;
            }
        }
        echo $permiso;
    }
}
