<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empleados extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,1);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }
    public function index(){
        $data['perfil']=$this->perfilid;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('empleados/empleados',$data);
        $this->load->view('templates/footer');
        $this->load->view('empleados/empleadosjs');
    }

    public function registra_empleado(){
        $data=$this->input->post();
        $personalId=$data['personalId'];
        if(isset($data['check_baja'])){
            $data['check_baja']='on';
        }else{
            $data['check_baja']='';
        }
        if($personalId==0){
            $data['reg']=$this->fecha_hora_actual;
            $id=$this->General_model->add_record('personal',$data);
        }else{
            $id=$this->General_model->edit_record('personalId',$personalId,$data,'personal');
            $id=$personalId;
        }
        echo $id;
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_empleados($params);
        $totaldata= $this->ModelCatalogos->total_empleados($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function delete_empleado(){
        $id=$this->input->post('id');
        $data = array('estatus'=>0);
        $this->General_model->edit_record('personalId',$id,$data,'personal');
    }
    public function registro_usuario(){
        $id=$this->input->post('id');
        $UsuarioID=0;
        $perfilId=0;
        $Usuario='';
        $pass='';
        $result_usuario=$this->General_model->getselectwhere('usuarios','personalId',$id);
        foreach ($result_usuario as $item){
            $UsuarioID=$item->UsuarioID;
            $perfilId=$item->perfilId;
            $personalId=$item->personalId;
            $Usuario=$item->Usuario;
            $pass='xxxxxxxxxx.';
        }
        $html='<form class="form" method="post" role="form" id="form_usuario">
                    <input type="hidden" name="UsuarioID" id="UsuarioID" value="'.$UsuarioID.'"> 
                    <div class="txt_usu">
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Nombre de usuario <div style="color: red" class="txt_usuario"></div></label>
                                  <input type="text" name="usuario" id="usuario" autocomplete="nope" oninput="verificar_usuario('.$UsuarioID.')" value="'.$Usuario.'" class="form-control">
                              </div>
                          </div>
                      </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Contraseña</label>
                                <input type="password" name="contrasena" id="contrasena" autocomplete="new-password" value="'.$pass.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Verificar contraseña</label>
                                <input type="password" name="contrasena2" id="contrasena2" value="'.$pass.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Empleado</label>';
                                $result=$this->General_model->getselectwhere('personal','personalId',$id);
                                $html.='<input type="hidden" name="personalId" readonly  class="form-control" value="'.$id.'">'; 
                                foreach ($result as $item) {
                                $html.='<input type="text" readonly class="form-control" value="'.$item->nombre.'">'; 
                                } 
                                $html.='
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Perfil</label>';
                                $resultp=$this->General_model->getselectwhere('perfiles','estatus',1);
                                $html.='<select name="perfilId" id="perfilId" class="form-control">';
                                foreach ($resultp as $item) {
                                    if($item->perfilId==$perfilId){
                                        $html.='<option value="'.$item->perfilId.'" selected>'.$item->nombre.'</option>';
                                    }else{
                                        $html.='<option value="'.$item->perfilId.'">'.$item->nombre.'</option>';
                                    }  
                                }
                                $html.='</select>
                        </div>
                        </div>
                    </div>   
                </form>';
        echo $html;
    }
    public function add_usuarios(){
        $datos = $this->input->post();
        $pss_verifica = $datos['contrasena'];
        $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
        $datos['contrasena'] = $pass;
        if($pss_verifica == 'xxxxxxxxxx.'){
           unset($datos['contrasena']);
        }
        $id=$datos['UsuarioID'];
        $personalId=$datos['personalId'];
        unset($datos['UsuarioID']);
        unset($datos['contrasena2']);
        if ($id>0) {
            $where = array('UsuarioID'=>$id);
            $this->General_model->edit_recordw($where,$datos,'usuarios');
            $result=2;
        }else{
            $this->General_model->add_record('usuarios',$datos);
            $result=1;
            $this->General_model->edit_recordw(array('personalId'=>$personalId),array('usuario'=>1),'personal');
        }   
        echo $result;
    }
    public function gafete($id){
        $data['get_empleado'] = $this->General_model->get_record('personalId',$id,'personal');
        $this->load->view('templates/header');
        $this->load->view('empleados/gafete',$data);
    }
    function validar(){
        $Usuario = $this->input->post('Usuario');
        $result=$this->General_model->getselectwhere('usuarios','Usuario',$Usuario);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }
    function cargafiles(){
        $id=$this->input->post('id');
        $folder="personal";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('foto'=>$newfile);
          $this->General_model->edit_record('personalId',$id,$array,'personal');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  
    
}