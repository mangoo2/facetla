<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unidades extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,2);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=2;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('unidades/listado');
        $this->load->view('templates/footer');
        $this->load->view('unidades/listadojs');
    }
    
    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_unidades($params);
        $totaldata= $this->ModelCatalogos->total_unidades($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function update_check(){
        $id=$this->input->post('id');
        $check=$this->input->post('check');
        $data = array('status'=>$check);
        $this->General_model->edit_record('UnidadId',$id,$data,'unidades');
    }
    
    

}    