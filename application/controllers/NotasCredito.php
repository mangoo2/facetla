<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotasCredito extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->fechahoy = date('Y-m-d');
        $this->fechahoyc = date('Y-m-d H:i:s');
        $this->fechahoyL = date('Y-m-d_H_i_s');
        $this->trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idsucursal=1;// esto sera por seccion para que cambien entre cuentas     xxx
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,2);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        redirect('NotasCredito/add');
    }
    function add($facturaId,$notaId=0){
        $data['notaId']=$notaId;
        $data['btn_active']=3;
        $data['btn_active_sub']=8;//numero de id submenu
        $data['fservicios']=$this->General_model->get_select('servicios',array('status'=>1));
        $data['funidades']=$this->General_model->get_select('unidades',array('status'=>1));
        $data['uso_cfdi']=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        $data['metodopago']=$this->General_model->get_select('f_metodopago',array('activo'=>1));

        $data['formapago']=$this->General_model->get_select('f_formapago',array('activo'=>1));
        $data['rowunidades']=$this->General_model->get_select('unidades',array('status'=>1));
        $data['rowservicios']=$this->General_model->get_select('servicios',array('status'=>1));
        $data['facturaId']=$facturaId;
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];

        $data["Nombrerasonsocial"]=$datosconfiguracion->Nombre;
        $data['rrfc']=$datosconfiguracion->Rfc;
        $data['regimenf']=$this->gt_regimenfiscal($datosconfiguracion->Regimen);

        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $data["nombrerasonsocialreceptor"]=$datosfactura->Nombre;
        $data["rfcreceptor"]=$datosfactura->Rfc;
        $data["fecha"]=$this->fechahoyc;
        $data["LugarExpedicion"]=$datosconfiguracion->CodigoPostal;
        $data["uuid_relacionado"]=$datosfactura->uuid;
        $data["Folio_relacionado"]=$datosfactura->Folio;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('notascredito/vista');
        $this->load->view('templates/footer');
        $this->load->view('notascredito/vistajs');
    }
    function gt_regimenfiscal($text){
          if($text="601"){ 
            $textl='<option value="601">601 General de Ley Personas Morales</option>';

          }elseif($text="603"){ 
            $textl='<option value="603">603 Personas Morales con Fines no Lucrativos</option>';

          }elseif($text="605"){ 
            $textl='<option value="605">605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>';

          }elseif($text="606"){ 
            $textl='<option value="606">606 Arrendamiento</option>';

          }elseif($text="607"){ 
            $textl='<option value="607">607 Régimen de Enajenación o Adquisición de Bienes</option>';

          }elseif($text="608"){ 
            $textl='<option value="608">608 Demás ingresos</option>';

          }elseif($text="609"){ 
            $textl='<option value="609">609 Consolidación</option>';

          }elseif($text="610"){ 
            $textl='<option value="610">610 Residentes en el Extranjero sin Establecimiento Permanente en México</option>';

          }elseif($text="611"){ 
            $textl='<option value="611">611 Ingresos por Dividendos (socios y accionistas)</option>';

          }elseif($text="612"){ 
            $textl='<option value="612">612 Personas Físicas con Actividades Empresariales y Profesionales</option>';

          }elseif($text="614"){ 
            $textl='<option value="614">614 Ingresos por intereses</option>';

          }elseif($text="615"){ 
            $textl='<option value="615">615 Régimen de los ingresos por obtención de premios</option>';

          }elseif($text="616"){ 
            $textl='<option value="616">616 Sin obligaciones fiscales</option>';

          }elseif($text="620"){ 
            $textl='<option value="620">620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos</option>';

          }elseif($text="621"){ 
            $textl='<option value="selected">selected="">621 Incorporación Fiscal</option>';

          }elseif($text="622"){ 
            $textl='<option value="622">622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>';

          }elseif($text="623"){ 
            $textl='<option value="623">623 Opcional para Grupos de Sociedades</option>';

          }elseif($text="624"){ 
            $textl='<option value="624">624 Coordinados</option>';

          }elseif($text="628"){ 
            $textl='<option value="628">628 Hidrocarburos</option>';

          }elseif($text="629"){ 
            $textl='<option value="629">629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales</option>';

          }elseif($text="630"){ 
            $textl='<option value="630">630 Enajenación de acciones en bolsa de valores</option>';
          }else{
            $textl='';
          }
          return $textl;
    }
}