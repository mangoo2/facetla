<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RetencionesAdicionales extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,2);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=4;
        $data['btn_active_sub']=9;
        $data['result']=$this->ModeloCatalogos->getselectwheren('f_retenciones',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('retencionesadicionales/vista');
        $this->load->view('templates/footer');
        $this->load->view('retencionesadicionales/vistajs');
    }
    function updatedatos(){
        $params = $this->input->post();
        $retencion=$params['retencion'];
        $estatus=$params['estatus'];
        $this->ModeloCatalogos->updateCatalogo('f_retenciones',array('estatus'=>$estatus),array('retencionId'=>$retencion));
    }
    
}