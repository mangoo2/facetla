<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloclientes');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,4);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cliente/listado');
        $this->load->view('templates/footer');
        $this->load->view('cliente/listadojs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;
        $data['clienteId']=$id;
        $data['uso_cfdi']=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cliente/vista');
        $this->load->view('templates/footer');
        $this->load->view('cliente/vistajs');
    }
    public function listado_factura($id){
        $data['btn_active']=2;
        $data['btn_active_sub']=4;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cliente/listadofactura');
        $this->load->view('templates/footer');
        //$this->load->view('cliente/vistajs');
    }

    public function getlistclientes() {
        $params = $this->input->post();
        $getdata = $this->Modeloclientes->get_list_clientes($params);
        $totaldata= $this->Modeloclientes->get_list_clientes_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function updatedatos(){
        $params = $this->input->post();
        $clienteId      =   $params['clienteId'];
        $correos        =   $params['correos'];
        unset($params['clienteId']);
        unset($params['correos']);

        if($clienteId>0){
            $this->ModeloCatalogos->updateCatalogo('clientes',$params,array('clienteId'=>$clienteId));
        }else{
            $clienteId=$this->ModeloCatalogos->Insert('clientes',$params);
        }
        $DATAc = json_decode($correos);       
        for ($i=0;$i<count($DATAc);$i++) {
            if ($DATAc[$i]->climailId>0) {
                
              $this->ModeloCatalogos->updateCatalogo('cliente_mail',array('mail'=>$DATAc[$i]->email),array('climailId'=>$DATAc[$i]->climailId));
            }else{
                $this->ModeloCatalogos->Insert('cliente_mail',array('mail'=>$DATAc[$i]->email,'clienteId'=>$clienteId));
            }
        }
    }
    function obtenerdatoscli(){
        $params = $this->input->post();
        $clienteId      =   $params['clienteId'];

        $result=$this->ModeloCatalogos->getselectwheren('clientes',array('clienteId'=>$clienteId));
        $emailsresult=$this->ModeloCatalogos->getselectwheren('cliente_mail',array('clienteId'=>$clienteId,'activo'=>1));
            $clienteId  =0;
            $razon_social ='';
            $rfc        ='';
            $direccion  ='';
            $banco='';
            $numero_cuenta='';
            $clabe='';
            $cp='';
            $RegimenFiscalReceptor='';
            $uso_cfdi='';
        foreach ($result->result() as $item) {
            $clienteId =$item->clienteId;
            $razon_social =$item->razon_social;
            $rfc =$item->rfc;
            $direccion =$item->direccion;
            $banco=$item->banco;
            $numero_cuenta=$item->numero_cuenta;
            $clabe=$item->clabe;
            $cp=$item->cp;
            $RegimenFiscalReceptor=$item->RegimenFiscalReceptor;
            $uso_cfdi=$item->uso_cfdi;
        }
        $data_select = array(
                            'clienteId' => $clienteId,
                            'razon_social'=>$razon_social,
                            'rfc'=>$rfc,
                            'direccion'=>$direccion,
                            'banco'=>$banco,
                            'numero_cuenta'=>$numero_cuenta,
                            'clabe'=>$clabe,
                            'cp'=>$cp,
                            'RegimenFiscalReceptor'=>$RegimenFiscalReceptor,
                            'uso_cfdi'=>$uso_cfdi,
                            'emails'=>$emailsresult->result()
                        );
        echo json_encode($data_select);
    }
    function deletemail(){
        $id = $this->input->post('id');

        $this->ModeloCatalogos->updateCatalogo('cliente_mail',array('activo'=>0),array('climailId'=>$id));
    }
    function deletecliente(){
        $id = $this->input->post('id');

        $this->ModeloCatalogos->updateCatalogo('clientes',array('activo'=>0),array('clienteId'=>$id));
    }
}    