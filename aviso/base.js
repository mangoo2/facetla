var tip, tipText, like, dislike, neutral, interval,
	initerCount = 0,
	initerMax = 5,
	database = null,
	tips = null,
	alertdiag = null,
	autoSaveConsultationDatesList = false,
	datepickersiniter = null,
	fileManagers = [],
	config = {
	    apiKey: SANDBOX ? "AIzaSyAjJ6niz4JzU2j6gMvCFHeqaLm85Lh7Y18" : "AIzaSyCbypZ6ZIJxjtAVYj3Yss4g_0wgtD--FPo",
	    authDomain: (SANDBOX ? "sandbox-" : "") +"overcomehelp.firebaseapp.com",
	    databaseURL: "https://"+(SANDBOX ? "sandbox-" : "")+"overcomehelp.firebaseio.com",
	    storageBucket: (SANDBOX ? "sandbox-" : "") + "overcomehelp.appspot.com",
	    messagingSenderId: SANDBOX ? "393125398139" : "953770269235"
	},
	signaturesFB = null,
	firebaseFunctions = SANDBOX ? "https://us-central1-sandbox-overcomehelp.cloudfunctions.net/" : "https://us-central1-overcomehelp.cloudfunctions.net/";

firebase.initializeApp(config);
database = firebase.database();    

String.prototype.hashCode = function() {
  var hash = 0, i, chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

$.fn.hasAttr = function(name) {  
   return this.attr(name) !== undefined;
};

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

String.prototype.hashCode = function(){
    var hash = 0;
    if (this.length == 0) return hash;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}

function utf8_encode (argString) { // eslint-disable-line camelcase
	  //  discuss at: https://locutus.io/php/utf8_encode/
	  // original by: Webtoolkit.info (https://www.webtoolkit.info/)
	  // improved by: Kevin van Zonneveld (https://kvz.io)
	  // improved by: sowberry
	  // improved by: Jack
	  // improved by: Yves Sucaet
	  // improved by: kirilloid
	  // bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
	  // bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
	  // bugfixed by: Ulrich
	  // bugfixed by: Rafał Kukawski (https://blog.kukawski.pl)
	  // bugfixed by: kirilloid
	  //   example 1: utf8_encode('Kevin van Zonneveld')
	  //   returns 1: 'Kevin van Zonneveld'

	  if (argString === null || typeof argString === 'undefined') {
	    return ''
	  }

	  // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
	  var string = (argString + '')
	  var utftext = ''
	  var start
	  var end
	  var stringl = 0

	  start = end = 0
	  stringl = string.length
	  for (var n = 0; n < stringl; n++) {
	    var c1 = string.charCodeAt(n)
	    var enc = null

	    if (c1 < 128) {
	      end++
	    } else if (c1 > 127 && c1 < 2048) {
	      enc = String.fromCharCode(
	        (c1 >> 6) | 192, (c1 & 63) | 128
	      )
	    } else if ((c1 & 0xF800) !== 0xD800) {
	      enc = String.fromCharCode(
	        (c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
	      )
	    } else {
	      // surrogate pairs
	      if ((c1 & 0xFC00) !== 0xD800) {
	        throw new RangeError('Unmatched trail surrogate at ' + n)
	      }
	      var c2 = string.charCodeAt(++n)
	      if ((c2 & 0xFC00) !== 0xDC00) {
	        throw new RangeError('Unmatched lead surrogate at ' + (n - 1))
	      }
	      c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000
	      enc = String.fromCharCode(
	        (c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
	      )
	    }
	    if (enc !== null) {
	      if (end > start) {
	        utftext += string.slice(start, end)
	      }
	      utftext += enc
	      start = end = n + 1
	    }
	  }

	  if (end > start) {
	    utftext += string.slice(start, stringl)
	  }

	  return utftext
	}

function utf8_decode (strData) { // eslint-disable-line camelcase
	  //  discuss at: https://locutus.io/php/utf8_decode/
	  // original by: Webtoolkit.info (https://www.webtoolkit.info/)
	  //    input by: Aman Gupta
	  //    input by: Brett Zamir (https://brett-zamir.me)
	  // improved by: Kevin van Zonneveld (https://kvz.io)
	  // improved by: Norman "zEh" Fuchs
	  // bugfixed by: hitwork
	  // bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
	  // bugfixed by: Kevin van Zonneveld (https://kvz.io)
	  // bugfixed by: kirilloid
	  // bugfixed by: w35l3y (https://www.wesley.eti.br)
	  //   example 1: utf8_decode('Kevin van Zonneveld')
	  //   returns 1: 'Kevin van Zonneveld'

	  var tmpArr = []
	  var i = 0
	  var c1 = 0
	  var seqlen = 0

	  strData += ''

	  while (i < strData.length) {
	    c1 = strData.charCodeAt(i) & 0xFF
	    seqlen = 0

	    // https://en.wikipedia.org/wiki/UTF-8#Codepage_layout
	    if (c1 <= 0xBF) {
	      c1 = (c1 & 0x7F)
	      seqlen = 1
	    } else if (c1 <= 0xDF) {
	      c1 = (c1 & 0x1F)
	      seqlen = 2
	    } else if (c1 <= 0xEF) {
	      c1 = (c1 & 0x0F)
	      seqlen = 3
	    } else {
	      c1 = (c1 & 0x07)
	      seqlen = 4
	    }

	    for (var ai = 1; ai < seqlen; ++ai) {
	      c1 = ((c1 << 0x06) | (strData.charCodeAt(ai + i) & 0x3F))
	    }

	    if (seqlen === 4) {
	      c1 -= 0x10000
	      tmpArr.push(String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF)))
	      tmpArr.push(String.fromCharCode(0xDC00 | (c1 & 0x3FF)))
	    } else {
	      tmpArr.push(String.fromCharCode(c1))
	    }

	    i += seqlen
	  }

	  return tmpArr.join('')
	}

function isMobile() {
	var mobile = false; //initiate as false
	// device detection
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
	    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
		mobile = true;
	}
	return mobile;
}

function updateAge(input){
	var d = new Date();
	var date = $(input).val().split("/");
	d.setYear(date[2]);
	d.setMonth(date[1]);
	d.setDate(date[0]);
	$("#age").val(calculateAge(d));
	return calculateAge(d);
}

function getQueryParameter(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function initNewLegalData(){
	if (getQueryParameter("updatelegal") == "true") {
		$.ajax({
			url: "/" + baseUrl() + "/legaldialog",
			type: "GET",
			success: function(response){
				var dialog = $(response);
				dialog.dialog({
					modal: true,
					autoOpen: true,
					width: 650,
					closeOnEscape: false,
					open: function() {
						var address = dialog.find(".address");
						createSignature("medicSignature");
						dialog.find("#specialisms").on("click", ".add", function(evt){
							evt.preventDefault();
							var fp = $(dialog).find("#specialisms .spetialism:first-child").clone();
							fp.find("input").val("");
							fp.find("select").val("");
							$(dialog).find("#specialisms").append(fp);
						});
						dialog.find("#specialisms").on("click", ".remove", function(evt){
							evt.preventDefault();
							if ($(dialog).find("#specialisms .spetialism").length > 1) {
								$(this).closest(".spetialism").remove();
							} else {
								$(this).closest(".spetialism").find("select,input").val("");
							}
						});
						address.find("[name=resume]").change(function(){
							var resume = $(this).val(),
							postalCode = address.find("[name=postalCode]").val(),
							city = address.find("[name=city]").val(),
							estate = address.find("[name=estate]").val();
						if (resume && !city && !estate) {
							$.ajax({
								url: "https://maps.googleapis.com/maps/api/geocode/json?address="+ encodeURI(resume) +"&key=" + API_KEY,
								success: function(response){
									console.log(response);
									if (response.status == "OK" && response.results && response.results.length > 0) {
										for (r in response.results){ 
											var components = response.results[r].address_components;
											for (c in components) {
												var ac = components[c], 
													types = ac.types.join("");
												try {
													if (types.indexOf("postal_code") >= 0) {
														address.find("[name=postalCode]").val(ac.long_name);
													} else if (types.indexOf("administrative_area_level_1") >= 0) {
														address.find("[name=estate]").val(ac.long_name);
													} else if (types.indexOf("locality") >= 0) {
														address.find("[name=city]").val(ac.long_name);
													}
												} catch (ex){ console.log(ex); }
											}
											break;
										}
									}
								}
							});
						}
					});
					},
					close: function(){
						$(this).dialog("destroy");
					},
					buttons: {
						"Aceptar": function() {
							var canvas = $("#medicSignature")[0],
								specialisms = dialog.find("#specialisms"),
								address = dialog.find(".address"),
								complete = true;
							
							specialisms.find(".spetialism").each(function(i, item){
								var sp = $(item).find("[name=specialism_name]").val().trim(),
									writ = $(item).find("[name=specialism_writ]").val().trim();
								if (!sp || !writ) {
									complete = false;
									return;
								}
							});
							
							address.find(".required").each(function(i, item){
								if (!$(item).val()) {
									complete = false;
									return;
								}
							});
							
							if (!complete) {
								alert("Debes especificar tu especialidad y tu cédula, así como proporcionar una dirección, ciudad y estado", "error");
							} else if ($("#accept").is(":checked") && !isCanvasBlank(canvas)) {
								var $dialog = $(this);
								sendSignature("medicSignature", $("#mk").val(), null, null, !$("#distribute").is("checked"), function(){
									$dialog.dialog("close");
								});
								
								dialog.find("#legalspecialism").ajaxSubmit({ url: "/" + baseUrl() + "/legaldialog", type: "POST" });
								
							} else if (!$("#accept").is(":checked")){
								alert("Debes aceptar el aviso de privacidad", "error");
								return false;
							} else {
								alert("Falta agregar una firma", "error");
								return false;
							}
						}
					}
				}).on('keydown', function(evt) {
					evt.stopPropagation();
			    });
			}
		});
	}
} 

function adjustTextareas(holder){
	if (!holder) {
		holder = $("body");
	}
	var textareas = holder.find("textarea").not(".iresize").not(".asinited");
	if (textareas.length > 0) {
		textareas.addClass("asinited");
		autosize.destroy();
		autosize(textareas);
		setTimeout(function(){ 
			autosize.update(textareas);
		}, 300);
		try{
			createRecordables();
		} catch (ex){}
	} else {
		setTimeout(function(){
			autosize.update(holder.find("textarea").not(".iresize"));
		}, 300);
	} 
}

function baseUrl() {
	var x = location.pathname,
		x = x.substring(1),
		slash = x.indexOf("/"),
		query = x.indexOf("?"),
		index = query > 0 ? ( slash > query ? query : slash ) : ( slash > 0 ? ( query < slash && query > 0 ? query : slash ) : x.length  ),
		x = x.substring(0, index);
	return x;	
}

function autosize_() {
	autosize.destroy();
	autosize($("textarea"));
}

function progressBar(evt){
	if (evt.lengthComputable) {
        var percentComplete = evt.loaded / evt.total;
        console.log("Porcentaje: " + percentComplete);
    }
}

function initClonables(area) {
	area.on("click", ".clonable .add", function(){
		var clonable = $(this).closest(".clonable"),
			holder = clonable.closest(".holder"),
			clon = clonable.clone();
		clon.removeAttr("id").find("select,input[type=text],input[type=number],textarea").val("");
		holder.append(clon);
		clon.find("input[type=text]").first().focus();
		holder.trigger("itemAdded");
		//area.dispatchEvent(new Event("cloned"));
	});
	area.on("keypress", ".clonable input", function(evt){
		var code = (evt.keyCode ? evt.keyCode : evt.which);
        if(code == 13){
        	var clonable = $(this).closest(".clonable");
			clonable.find(".add").trigger("click");
        }
	});
		
	area.on("click", ".clonable .delete", function(){
		var clonable = $(this).closest(".clonable"),
			holder = clonable.closest(".holder");
		if (clonable.siblings(".clonable:visible").length == 0) {
			clonable.removeAttr("id").find("select,input[type=text],textarea").val("");
		} else {
			clonable.remove();
		}
		holder.trigger("itemRemoved");
	});
}

function calculateAge(birthday) { // birthday is a date
    return birthday ? ~~((Date.now() - birthday) / (31557600000)) : "";
}


function alert(text, clazz){
	if (!alertdiag) {
		alertdiag = $("<div id='alertdiag' class='alertMsg "+(clazz ? clazz : 'ok' )+"' style='display:none;'>" + text + "</div>");
	} else {
		alertdiag.attr("class", "alertMsg " + (clazz ? clazz : 'ok'));
		alertdiag.text(text);
	}
	$("body").append(alertdiag);
	alertdiag.show("fade");
	setTimeout(function(){ alertdiag.fadeOut(1000, function() { alertdiag.remove(); }) }, 3500);
}

function initRastreables(){
	$("body").on("click", ".rastreable", trackRastreable);
}

function trackRastreable(evt, rastreable){
	try {
		rastreable = rastreable ? rastreable : $(this); 
		var events = rastreable.data("event"),  
			evts = events ? events.split(",") : [];
		//if (($.inArray(evt.type, evts) > -1) && rastreable.data("category")) {
			ga('send', 'event', rastreable.data("category") || "", rastreable.data("event") || evt.type || "", rastreable.data("label") || "");
		//}
	} catch (ex){}
}

function getCurrentDate(){
	var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    return today = dd+'/'+mm+'/'+yyyy;
}

$.datepicker.regional['es'] = {
	 closeText: 'Cerrar',
	 prevText: '<Ant',
	 nextText: 'Sig>',
	 currentText: 'Hoy',
	 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
	 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
	 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
	 weekHeader: 'Sm',
	 dateFormat: 'dd/mm/yy',
	 firstDay: 1,
	 isRTL: false,
	 showMonthAfterYear: false,
	 yearSuffix: ''
 };

$.datepicker.setDefaults($.datepicker.regional['es']);

$(function() {
	var window_height = $(window).height(),
		header = $("header").outerHeight(true),
		footer = $("footer").outerHeight(true),
		hmenu = $("nav.hmenu").outerHeight(true),
		tools = $("div#tools").outerHeight(true),
		body = $("body");
	var totalArticle = window_height - footer - header - (hmenu ? hmenu : 0) - (tools ? tools : 0);
	if ($(".login").length == 0) {
		$(window).on('resize', function(){
			window_height = $(window).height(),
			header = $("header").outerHeight(true),
			footer = $("footer").outerHeight(true),
			hmenu = $("nav.hmenu").outerHeight(true),
			tools = $("div#tools").outerHeight(true),
			body = $("body");
			body.css("padding-top", (header + tools) + "px");
			$("article").add(".content").css('min-height', totalArticle + "px");
		});
		body.css("padding-top", (header + tools) + "px");
		$("article").add(".content").css('min-height', totalArticle + "px");
	} 
	body.on("keydown", "input[type=number]", allowNumbersOnly);
	body.on("keydown", "input[type=tel]", allowNumbersOnly);
	
	$.ajaxSetup({
		complete: function(xhr, status) {
			try {
				if (xhr.responseText == 'invalid' || xhr.responseText.indexOf("<title>Login Eleonor</title>") > 0 ) { window.location = "/login"; }
			} catch (ex){}
		}
	});
	$(document).on({
		ajaxStart : function() {
			body.addClass("loading");
		},
		ajaxStop : function() {
			body.removeClass("loading");
		},
		ajaxError: function(xhr, ajaxOptions, thrownError){
			console.log("Error:" + thrownError);
			body.removeClass("loading");
			if (ajaxOptions.status != 0 && ajaxOptions.status != 200) {
				alert("Hubo un error de comunicación. Por favor verifique su internet e intente nuevamente", "error");
			}
		}
	});
	
	 body.on("click", "a,input[type=button],button", function(e){
	    var $link = $(e.target);
	    if(!$link.data('lockedAt') || +new Date() - $link.data('lockedAt') > 300) {
	    	console.log(">continue");
	    } else {
	    	console.log(">preventing");
	    	try { 
	    		e.preventDefault();
	    	} catch (ex){}
	    	try {
	    		e.stopPropagation();
	    	} catch (ex){}
	    	try {
		    	e.cancelBubble = true;
		    } catch (ex){}
			return false;
	    }
	    $link.data('lockedAt', +new Date());
	});
	
	$(".hour").mask("99:99");
	$(".cardnumber").mask("9999-9999-9999-9999");
	$(".clabe").mask("9999-9999-9999-9999-99");
	$("body").on("paste", "textarea:not(.iresize)", function(evt){
		autosize.update($(this));
	});
	
	$("body").on("blur", "input[type=email]", function(){
		if (!$(this).hasClass("ignore") && !validMail($(this))) {
			alert("Por favor, verifique el correo electrónico", "error");
			$(this).focus();
		}
	});
	$("body").on("change", "input[type=number]", function(){
		var input = $(this),
			val = parseInt(input.val().trim()),
			max = input.attr("max"),
			min = input.attr("min");
		if (max && val > max) {
			input.val(max);
		} else if (min != null && val < min) {
			input.val(min);
		}
	});
	var phoneLenght = $("body").data("phonelength") || "10";
	$("input[type=tel]").attr("pattern", "[0-9]{"+phoneLenght+"}").attr("maxlength", phoneLenght);
	body.on("paste", "input[type=tel]", function(e){
		e.clipboardData.getData('text/plain').slice(0, phoneLenght);
	})
	
	$("input[type=checkbox]").addClass("form-checkbox");
	$("input[type=radio]").addClass("form-radioc");
	$("input").attr("autocomplete","off");
	$("body").on("focus", "input[type=tel]", function(){
		if (!$(this).hasClass("tinited")) {
			var phoneLenght = $("body").data("phonelength") || "10";
			$(this).addClass("tinited");
			$(this).attr("pattern", "[0-9]{"+phoneLenght+","+phoneLenght+"}").attr("maxlength", phoneLenght);
		}
	});
	$("body").on("blur", "input[type=tel]", function(){
		var val = $(this).val().trim().length,
			phoneLenght = $("body").data("phonelength") || "10";
		if (val > 0 && val != parseInt(phoneLenght)) {
			alert("Debe ingresar el número telefónico con lada (LADA+Teléfono (Ejemplo: 222784585) "+phoneLenght+" dígitos. Sin espacios, guión o paréntesis)", "error");
			$(this).focus();
		}
	});
	$("input[type=number]").not(".fixstep").attr("step", "any");
	body.on("click", "a.post", function(e) {
		e.stopPropagation();
		e.preventDefault();
		var href = this.href;
		var parts = href.split('?');
		var url = parts[0];
		var params = parts[1].split('&');
		var pp,
			inputs = '';
		for (var i = 0, n = params.length; i < n; i++) {
			pp = params[i].split('=');
			inputs += '<input type="hidden" name="' + pp[0] + '" value="' + pp[1] + '" />';
		}
		$("body").addClass("loading").append('<form action="' + url + '" method="post" id="poster">' + inputs + '</form>');
		$("#poster").submit();
		$("body").addClass("loading");
	});
	
	body.find("a.loadingBody").click(function(){
		body.addClass("loading");
	});

	/**Tutorials**/
	var expediente = $('#expedient'),
		tabActive = expediente.find('[role="tab"].ui-state-default.ui-corner-top.ui-tabs-active.ui-state-active').find('a').attr('href');
	if (expediente.length) {
		switch (tabActive) {
		case '#history':
			$('#btnTuto').hide();
			break;
		case '#consultationList':
			$('#btnTuto').show();
			break;
		case '#files':
			$('#btnTuto').hide();
			break;
		case '#medicalReport':
			$('#btnTuto').hide();
			break;
		}
	}
	else if ($('#patientRegister').length) {
		$('#btnTuto').show();
	}
	else {
		$('#btnTuto').hide();
	}
	
	
	$('#btnTuto').on("click", function(event) {
		misTours.removeTourAll();
		var expediente = $('#expedient'),
			tabActive = expediente.find('[role="tab"].ui-state-default.ui-corner-top.ui-tabs-active.ui-state-active').find('a').attr('href');
		if (expediente.length) {
			switch (tabActive) {
			case '#history':
				misTours.buttonTuto("tourClinicHistory");
				misTours.initTour("tourClinicHistory");
				misTours.removeTour("tourBotonNuevoConsulta");
				break;
			case '#consultationList':
				misTours.buttonTuto("tourBotonNuevoConsulta");
				misTours.initTour("tourBotonNuevoConsulta");
				break;
			case '#files':
				alert("No hay una guía para esta pantalla","error");
				break;
			case '#medicalReport':
				alert("No hay una guía para esta pantalla","error");
				break;
			}
		}else if($('#patientRegister').length){						
				$("#patientProfile").dialog("close");
				misTours.buttonTuto("pantallaPrincipalRegistrarPaciente");
				misTours.initTour("pantallaPrincipalRegistrarPaciente");
		}else{
			alert("No hay una guía para esta pantalla","error");
		}
		
	});
	
	/**Tutorials**/
    
    initDatePickers();
    initRastreables();
   
    $('#consultationDates, #todayLink').on("click", function(){
    	try{
    		if(saveConsultation == "consultation"){
    			autoSaveConsultationDatesList = true;	
    			autosaveConsultation();
    			saveConsultation = null;
    		}
    	}catch(e){
    	}
    });
    autosize($('textarea').not(".iresize"));
    
    $("body").on("focus", ".warning", function() { $(this).removeClass("warning") });
    
    $("body").on("blur", "input[type=text],input[type=search]", function(){
    	var text = $(this).val().trim().toLowerCase();
    		clear = false;
    	switch (text) {
    		case "harlemshake": doHarlemShake(); clear = true; break;
    		case "doabarrelroll": doABarrelRoll(); clear = true; break;    		
    	}
    	if (clear) {
    		$(this).val("");
    	}
    });
    
    initNewLegalData();
    
    try {
		moment.locale("es");
		moment.tz.setDefault("America/Mexico_City");
	} catch(ex){}
});

function reloadPage(){
	try {
		window.location.reload(true); 
	} catch (exe) {
		console.log("Error al tratar de realizar un reload");
		try {
			var url = new URL(window.location.href),
				mk = $(this).val();
			url.searchParams.set("mk", mk);
			window.location.href = url.href;
		} catch (ex){
			try {
				window.location = window.location.pathname + "?mk=" + mk;
			} catch (ex){
				location.reload();
			}
		}
	}
}

function getQueryParameters(str) {
    return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
}

function initDatePickers(){
	if (datepickersiniter) {
		clearInterval(datepickersiniter);
	}
	datepickersiniter = setTimeout(function(){
		try {
	    	$("input[type=date]").prop("type", "text").addClass("dpkr");
	    	$("input.dpkr").not(".ignore").attr("placeholder", "dd/mm/yyyy").datepicker({
	  		  dateFormat: "dd/mm/yy",
	  		  changeMonth: true,
	  	      changeYear: true,
	  	      yearRange: "-50:+50",
	  	      showMonthAfterYear: true,
	  	      gotoCurrent: true,
	  	      onChangeMonthYear: function(year, month, inst) {
	  	    	  //console.log("Inst: " + inst);
	  	    	  var day = writableDate(2, inst.selectedDay),
	  	    	  	month = writableDate(2, inst.selectedMonth + 1),
	  	    	  	callback = $(this).data("callback");
	  	    	  $(this).val(day + "/" + month + "/" + inst.selectedYear);
	  	    	  if (callback) {
	  	    		  eval(callback + "(this)");
	  	    	  }
	  	      } 
	    	}).each(function(i, item){
	    		try {
		    		if ($(item).val()){
		    			$(item).attr("value", $(item).val());
		    		}
		    		if ($(item).val()){
		    			$(item).datepicker("setDate", $(item).val());
		    		}
	    		} catch (ex){
	    			console.log(ex);
	    		}
	    	});
	    	$("input.dpkr").mask("99/99/9999",{placeholder:"dd/mm/yyyy"});
	    	$("input.dpkr.birthdate").datepicker("option", "defaultDate", "1/1/1960");
	    	$("input.dpkr.birthdate").datepicker("option", "yearRange", "-100:+0");
	    	$("input.dpkr.before")
	    		.datepicker( "option", "maxDate", "+0d" )
	    		.datepicker( "option", "yearRange", "-100:+0");
	    	$("input.dpkr.after")
	    		.datepicker( "option", "minDate", "-0d" )
	    		.datepicker( "option", "yearRange", "+0:+100");
	    		;
	    	$("input.dpkr.birthdate.register").attr("placeholder", "Fecha de nacimiento");
	    } catch (ex) {
	    	console.log("Error al crear datePicker");
	    }
	}, 200);
}

function getCleanText(holderTag) {
	 return $(holderTag).text().replaceAll(/\s+/, "+").replaceAll(/\+/, " ");
}

function omitirAcentos(text) {
    var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç",
    	original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
    	re = null;
    for (var i=0; i<acentos.length; i++) {
    	re = new RegExp(acentos.charAt(i), "g")
        text = text.replace(re, original.charAt(i));
    }
    return text;
}

function writableDate(spaces, number) {
	if (number < 10) {
		return "0" + number;
	} else {
		return "" + number;
	}
}

function allowNumbersOnly(event){
	var key = event.charCode || event.keyCode || 0,
	value = $(this).val(),
	control = window.event.ctrlKey || event.ctrlKey;
	if(control || (key == 9 || key == 17 || key == 91 || key == 8 || key == 37 || key == 39 || key == 38 || key == 40 || key == 46 || ((key == 190 || key == 110) && value.indexOf('.') == -1))){ 
		return true;
	} else if ((key < 48 || key > 57) && (key < 96 || key > 105)) {
		event.preventDefault();
	}
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function registerValidation(selector, formButtonSelector) {
	selector = selector ? selector : document;
	$(selector).on('click', formButtonSelector, function(e) {
	    if(!isValidForm()) {
	    	e.preventDefault(); 
	    }
	});
}

function updateMedicCID(medicInfo){
	console.log(">Actualizando en firebase la información del médico: " + JSON.stringify(medicInfo));
	try {
		$.ajax({
			type: "POST",
			url: firebaseFunctions + "updateCatalogue",
			global: false,
			data: JSON.stringify(medicInfo),
			contentType: "text/plain;charset=UTF-8",
			processData: false,
			success: function(cid) {
				if (cid) {
					$.ajax({ 
						type: "POST",
						global: false,
						url: "/medic/updatecid",
						data: { mk: medicInfo.mk, cid: cid }
					});
				} else {
					console.log("> Error al obtener el CID del médico");
				}
			},
			error: function(res) {
				console.log(">Erorr al actualizar firebase: " + res);
			}
		});
	} catch (ex){
		console.log(ex);
	}
}

function verifyMail(input, callback){
	var mail = input.val(),
		rol = input.siblings("#rol").val(),
		fields = input.siblings("#fields").val();
	input.removeClass("invalid").removeClass("valid");
	$.ajax({
		url: "/check",
		data: { m : mail, r: rol, f:fields },
		success: function(response){
			if (response.user) {
				if (response.rol) {
					input.addClass("invalid");
				} else {
					input.addClass("valid");
				}
			} else {
				input.addClass("valid");
			}
			if (callback) {
				callback(response);
			}
		}
	});
}

function resetFormValidation(selector) {
	var form = selector ? $(selector) : $("body");
	form.find("input").add(form.find("select")).add(form.find("textarea")).each(function(i, item){
		$(item).removeClass("warning");
	});
}

function isValidForm(selector, ignoreHidden){
	var valid = true,
		form = selector ? $(selector) : $("body"),
		invalid = [];
	$(form.find("input")).add(form.find("select")).add(form.find("textarea")).each(function(i, item){
		item = $(item);
		item.removeClass("invalid");
		if (!ignoreHidden || item.is(":visible")){	
			var itemType = item.attr("type"),
				itemVal = itemType == "checkbox" ? item.is(":checked") : item.val() ? item.val().trim() : "";
			if (item.hasAttr("required") &&!item.hasClass("ignore") && !itemVal) {
				invalid.push(item);
				valid = false;
			} 
			if (item.hasClass("invalid")) {
				invalid.push(item);
				valid = false;
			}
			if (itemVal) {
				if (itemType == "email" && !(/\S+@\S+\.\S+/.test(itemVal))) {
					invalid.push(item);
					valid = false;
				}
				if (itemType == "number" && !isNumeric(itemVal)) {
					invalid.push(item);
					valid = false;
				}
				if (itemType == "tel" && !(itemVal.match(/\d/g).length===parseInt($("body").data("phonelength") || "10"))) {
					invalid.push(item);
					valid = false;
				}
				if (item.hasClass("dpkr") && !(/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/).test(itemVal)) {
					invalid.push(item);
					valid = false;
				} 
				if (item.hasClass("hour") && !itemVal.replace("__:__", "").trim()) {
					invalid.push(item);
					valid = false;
				}
			}
		}		
	});
	$(invalid).each(function(i, item){
		$(item).addClass("warning");
	});
	if (!valid) {
	try {
		var first = invalid[0],
			top = first.scrollTop(); 
		$(window).scrollTop(top);
		first.focus();
	} catch (exc) {}
	 	//alert("Faltan campos por llenar, por favor revise el formulario", "error");
	}
	return valid;
}

  
  $(function(){
	  tip = $("#tip");
	  tipText =tip.find("#tipText");
	  like = tip.find("#like");
	  dislike = tip.find("#dislike");
	  neutral = tip.find("#neutral");
	  tip.show();
	  
	  if (tips == undefined) {
		  database.ref('/tipsEleonor').once('value', function(snapshot) {
			  tips = snapshot.val();
			  showTips(tips);
		  });
	  }
	  
  }); 

function showTips(tips){
    if (interval) {
    	clearInterval(interval);
    }
	if(tipText.is(":visible")){
      tipText.hide("fade",function(){
        makeMagic();
      });
    } else {
      makeMagic();
    }
    interval = setInterval(function(){
    	tipText.hide("slide",function(){
    		makeMagic();
    	});
    }, 12000);
}
function hideTips(){
	tip.hide();
}
function makeMagic(){
	var tipNumber = Math.floor((Math.random() * 40) + 1);
	    tipText.text(tips["tip"+tipNumber].text);
	    like.data( "id", "tip"+tipNumber );
	    dislike.data( "id", "tip"+tipNumber );
	    neutral.data( "id", "tip"+tipNumber );
	    activatedAll();
	if (!$("body").hasClass("sandbox")) {
	    like.one("click", sendLike);
	    dislike.one("click", senddislike);
	    neutral.one("click", sendNeutral);
	    tipText.show("fade", function(){
			if (tipText.is(":visible")) {
		    	firebase.database().ref("/tipsEleonor/tip"+tipNumber+"/shown").transaction(function(val){
					return val + 1;
				});
			}
		});
	}	
}
function sendLike(event){
	var id= $(this).data("id"),
	likeCounter = tips[id].like + 1; 
	firebase.database().ref("/tipsEleonor/"+id).update({
		"like":likeCounter
	});
	clearInterval(interval);
	showTips(tips);
}
function senddislike(event){

	 var id= $(this).data("id"),
	dislikeCounter = tips[id].dislike + 1; 
	database.ref("/tipsEleonor/" + id).update({
	  "dislike":dislikeCounter
	});
	clearInterval(interval);
showTips(tips);
}
function sendNeutral(event){
	var id= $(this).data("id"),
	neutralCounter = tips[id].neutral + 1; 
	database.ref("/tipsEleonor/" + id).update({
	  "neutral":neutralCounter
	});
	clearInterval(interval);
	showTips(tips);
}
function activatedAll(){
	like.unbind( "click" );
	dislike.unbind( "click" );
	neutral.unbind( "click" );
	$("#like, #dislike, #neutral").removeClass("disabledFeedBackTip");
}

function validMail(mailField){
	var mail = mailField.val().toLowerCase(),
		re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	mailField.val(mail);
	return mail ? re.test(mail) : true; 
}
function downloadCSV(csv, filename) {
	 var csvFile;
		var downloadLink;

		// CSV file
		csvFile = new Blob([ csv ], {
			type : "text/csv"
		});

		var universalBOM = "\uFEFF";
		
		// Download link
		downloadLink = document.createElement("a");

		if(!filename.includes(".csv")){
			filename += ".csv";
		}
		// File name
		downloadLink.download = filename;

		// Create a link to the file
		//downloadLink.href = window.URL.createObjectURL(csvFile);
		downloadLink.setAttribute('href', 'data:text/csv;charset=utf-8,%EF%BB%BF' + encodeURIComponent(csv));

		// Hide download link
		downloadLink.style.display = "none";

		// Add the link to DOM
		document.body.appendChild(downloadLink);

		// Click download link
		downloadLink.click();
}

/*función que copia textos de un elemento*/
function copyToClipboard (element) {
	var $temp = $("<input type='text' value='"+ $(element).text() + "'>"),
	 	textToCopy = $(element).text(),
	 	tries = 0;
	
	navigator.permissions.query({name: "clipboard-write"}).then(function(result) {
	  if (result.state == "granted" || result.state == "prompt") {
		navigator.clipboard.writeText(textToCopy).then(function(){
			alert("El texto se copió al portapapeles");
			console.log("Se copió al portapapeles desde then: " + $(element).text());
		}).catch(function(){
			$("body").append($temp);
			$temp.val(textToCopy);
			try{ 
				$temp.select();
			} catch (ex){}
			try{ 
				$temp[0].select();
			} catch (ex){}
			try{ 
				$temp.setSelectionRange(0, 99999);
			} catch (ex){}
			try{ 
				$temp[0].setSelectionRange(0, 99999);
			} catch (ex){}
			
			document.execCommand("copy");
			$temp.remove();
			console.log("Se copió al portapapeles desde catch 1:" + $(element).text());
			alert("El texto se copió al portapapeles");
		});
	  }
	}).catch(function(){
		$("body").append($temp);
		$temp.val(textToCopy);
		try{ 
			$temp.select();
		} catch (ex){}
		try{ 
			$temp[0].select();
		} catch (ex){}
		try{ 
			$temp.setSelectionRange(0, 99999);
		} catch (ex){}
		try{ 
			$temp[0].setSelectionRange(0, 99999);
		} catch (ex){}
		
		document.execCommand("copy");
		$temp.remove();
		console.log("Se copió al portapapeles desde catch 2:" + $(element).text());
		alert("El texto se copió al portapapeles");
	});
}

